# 🔌 `LazyLoader` component

> Load a component on demand with `import()`.

## Abstract

- the `promise` `prop` is a function that returns the import
- the loaded component's default export is rendered with
  the loader `prop`s with the exception of `promise`

## Example

    import { createElement } from 'react';
    import { LazyLoader } from '@mintlab/ui';
    
    export const FooOnDemand = props =>
      createElement(LazyLoader, {
        promise: () => import('./Foo'),
        ...props,
      });

## Pitfalls

- the import path **must** be a string literal
- `import()` is **not** a function

## See also

- [`LazyLoader` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/LazyLoader)
- [`LazyLoader` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-lazyloader)
- [TC39 `import()` proposal](https://tc39.github.io/proposal-dynamic-import/)