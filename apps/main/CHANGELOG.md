# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.10.0...@zaaksysteem/main@0.11.0) (2019-09-09)


### Bug Fixes

* **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))


### Features

* **Apps:** Implement typescript ([583155f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/583155f))
* **Note:** Implement typescript ([47808bc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/47808bc))
* **SwitchViewButton:** Implement typescript ([b119c2d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b119c2d))
* **Tabs:** Implement typescript ([7eabf36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7eabf36))
* **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))
* **ThreadPlaceholder:** Implement typescript ([be532c7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/be532c7))
* **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))





# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.1...@zaaksysteem/main@0.10.0) (2019-09-06)


### Bug Fixes

* **Communication:** restrict rendering contentarea when on threads-only route ([747c4ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/747c4ce))


### Features

* **Communication:** MINTY-1289 Add link to case when thread is displayed in the context of a contact ([ed0bd1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ed0bd1e))
* **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))
* **Communication:** MINTY-1402 - add support for saving a note ([4fd9e80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4fd9e80))





## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.0...@zaaksysteem/main@0.9.1) (2019-09-05)

**Note:** Version bump only for package @zaaksysteem/main





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.1...@zaaksysteem/main@0.9.0) (2019-08-29)


### Bug Fixes

* **Communication:** fix invalid value when saving a contact ([4ea871b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4ea871b))
* **Communication:** MINTY-1386: fix uuid of contact when saving ([771f520](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/771f520))
* **Communication:** MINTY-1391 - display thread content with newlines ([f754156](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f754156))
* **Communication:** Update thread unwrapping to the fixed API ([3604122](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3604122))


### Features

* **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
* **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
* **Communication:** Add view for notes ([4481b91](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4481b91))
* **Communication:** change breakpoints so medium will show thread list ([1239fba](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1239fba))
* **Communication:** Implement API v2 for threadview ([c5f2689](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5f2689))
* **Communication:** Implement placeholder for the view side ([4271423](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4271423))
* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
* **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
* **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
* **Communication:** MINTY-1115 Move add and refresh button to `Threads` list ([e806749](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e806749))
* **Communication:** MINTY-1126 Style add button ([594ab7a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/594ab7a))
* **Communication:** MINTY-1260 - save Contactmoment, misc. styling and refactoring ([1c1b872](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c1b872))
* **Communication:** MINTY-1289 Add communication module to contact route ([5ab6ba4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ab6ba4))
* **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
* **Communication:** tweak some misc. styling ([e1a16b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1a16b5))
* **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
* **ErrorHandling:** MINTY-1126 Centralized error handling displaying a `Alert` for every failed async action ([d65d459](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d65d459))
* **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))





## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.0...@zaaksysteem/main@0.8.1) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/main





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.2...@zaaksysteem/main@0.8.0) (2019-08-22)


### Features

* **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))





## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.1...@zaaksysteem/main@0.7.2) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.0...@zaaksysteem/main@0.7.1) (2019-08-06)

**Note:** Version bump only for package @zaaksysteem/main





# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.6.0...@zaaksysteem/main@0.7.0) (2019-08-01)


### Features

* **Iframe:** MINTY-1121 When running in an iframe, notify top window of location changes ([4618672](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4618672))





# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.5.0...@zaaksysteem/main@0.6.0) (2019-07-31)


### Features

* **Communication:** MINTY-1126 Add case reducer which provides information needed for communication module & add temporary dashboard, useful for navigating to open cases ([6d70c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d70c7d))





# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.1...@zaaksysteem/main@0.5.0) (2019-07-31)


### Features

* **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))





## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.0...@zaaksysteem/main@0.4.1) (2019-07-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.3.0...@zaaksysteem/main@0.4.0) (2019-07-29)


### Features

* **Dates:** MINTY-1120 Add localized date formatting lib `fecha` ([84b6693](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84b6693))





# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.2.0...@zaaksysteem/main@0.3.0) (2019-07-29)


### Features

* **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))





# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.1.0...@zaaksysteem/main@0.2.0) (2019-07-25)


### Features

* **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))





# 0.1.0 (2019-07-25)


### Features

* **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))





## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@zaaksysteem/case-management@0.1.0...@zaaksysteem/case-management@0.1.1) (2019-07-23)

**Note:** Version bump only for package @zaaksysteem/case-management





# 0.1.0 (2019-07-18)


### Features

* **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
* **Setup:** MINTY-1120 Add eslint and prettier ([0e6b94c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/0e6b94c))
* **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
* **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
* **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
