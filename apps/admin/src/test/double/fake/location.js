/**
 * Fake method for navigation (not implemented by JSDOM).
 *
 * @param {string} url
 */
function assign(url) {
  window.history.pushState(null, '', url);
}

module.exports = assign;
