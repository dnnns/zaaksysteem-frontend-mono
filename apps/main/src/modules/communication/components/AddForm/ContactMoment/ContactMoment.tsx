import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/styles';
import classNames from 'classnames';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
import * as validationRules from '@zaaksysteem/common/src/components/form/validation/validationRules';
import {
  FormDefinition,
  FormRendererRenderProps,
  FormRendererFormField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
//@ts-ignore
import { applyTranslations } from '@zaaksysteem/common/src/library/applyTranslations';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { Button } from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Render from '@mintlab/ui/App/Abstract/Render';
import ContactFinder from '../ContactFinder/ContactFinder';
import CaseFinder from '../CaseFinder/CaseFinder';
import { addFormStyleSheet } from './../AddForm.style';
import {
  CONTACT_FINDER,
  CASE_FINDER,
} from '../../../library/communicationFieldTypes.constants';
import { TYPE_CONTACT_MOMENT } from '../../../library/communicationTypes.constants';

const renderField = ({ classes }: { classes: { [key: string]: string } }) => {
  return function RenderFieldComponent({
    FieldComponent,
    name,
    error,
    touched,
    ...rest
  }: FormRendererFormField) {
    const props = {
      ...cloneWithout(rest, 'type', 'mode', 'classes'),
      compact: true,
      name,
      key: `contact-moment-component-${name}`,
      scope: `contact-moment-component-${name}`,
    };

    return (
      <div
        className={classNames(classes.formRow, {
          [classes.formRowBackground]:
            rest.type === CONTACT_FINDER || rest.type === CASE_FINDER,
        })}
        key={props.key}
      >
        <FieldComponent {...props} />
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
};

const typeMap = {
  [CONTACT_FINDER]: ContactFinder,
  [CASE_FINDER]: CaseFinder,
};

const validationMap = {
  [CONTACT_FINDER]: validationRules.selectRule,
  [CASE_FINDER]: validationRules.selectRule,
};

export interface ContactMomentProps {
  save: (options: { [key: string]: any }) => void;
  busy: boolean;
  classes: any;
  formDefinition: FormDefinition;
  rootPath: string;
}

const ContactMoment: React.FunctionComponent<ContactMomentProps> = ({
  save,
  busy,
  classes,
  formDefinition,
  rootPath,
}) => {
  const [t] = useTranslation();
  const translatedFormDefinition: FormDefinition = applyTranslations(
    formDefinition,
    t
  );
  const renderForm = ({ fields, values, isValid }: FormRendererRenderProps) => {
    const formFields = fields.map(renderField({ classes }));

    const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
      <Link innerRef={ref} to={rootPath} {...props} />
    ));
    LinkComponent.displayName = 'cancelButtonLinkComponent';

    return (
      <div className={classes.formWrapper}>
        {formFields}

        <div className={classes.buttons}>
          <Button component={LinkComponent} presets={['text', 'contained']}>
            {t('common:forms.cancel')}
          </Button>
          <Button
            disabled={!isValid || busy}
            action={() => {
              save({
                type: TYPE_CONTACT_MOMENT,
                values,
              });
            }}
            presets={['primary', 'contained']}
          >
            {t('common:forms.add')}
          </Button>
        </div>
      </div>
    );
  };

  return (
    <FormRenderer
      formDefinition={translatedFormDefinition}
      FieldComponents={typeMap}
      validationMap={validationMap}
      isInitialValid={false}
    >
      {renderForm}
    </FormRenderer>
  );
};

export default withStyles(addFormStyleSheet)(ContactMoment);
