const BASE = 16;
let INDEX = 0;

/**
 * Generate an identifier from
 * - customizable prefix
 * - hexadecimal timestap
 * - local incrementor
 * that is
 * - cheap
 * - platform agnostic
 * - unique enough to prevent accidental collision
 *
 * @param {string} [prefix=zs]
 * @return {string}
 */
export function unique(prefix = 'zs') {
  const hexadecimalTimestamp = new Date().getTime().toString(BASE);
  const index = INDEX++;

  return [prefix, hexadecimalTimestamp, index].join('-');
}
