import React from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { withStyles } from '@material-ui/styles';
//@ts-ignore
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import Render from '@mintlab/ui/App/Abstract/Render';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { messageHeaderStyleSheet } from './MessageHeader.style';

const formatDate = (t: i18next.TFunction, zulu: string) => {
  const newDate = new Date(zulu);
  const date = fecha.format(newDate, t('dates.dateFormatText'));
  const time = fecha.format(newDate, t('dates.timeFormat'));

  return `${date} ${time}`;
};

type MessageHeaderPropsType = {
  classes: any;
  date: string;
  title: string;
  info?: string;
  icon: string;
  caseNumber?: string;
};

const MessageHeader: React.FunctionComponent<MessageHeaderPropsType> = ({
  classes,
  date,
  title,
  info,
  icon,
  caseNumber,
}) => {
  const [t] = useTranslation('common');
  const handleCaseLinkClick = (event: any) => {
    preventDefaultAndCall(() => window.top.location.assign(event.target.href))(
      event
    );
  };

  return (
    <div className={classes.header}>
      <div className={classes.icon}>
        <ZsIcon size={48}>{icon}</ZsIcon>
      </div>
      <div className={classes.meta}>
        <div className={classes.title}>{title}</div>
        <Render condition={info}>
          <div className={classes.info}>{info}</div>
        </Render>
      </div>
      <div className={classes.right}>
        <div className={classes.date}>{formatDate(t, date)}</div>
        {caseNumber && (
          <a
            className={classes.link}
            onClick={handleCaseLinkClick}
            href={`/intern/zaak/${caseNumber}`}
          >
            {t('communication:thread.linkToCase', { caseNumber })}
          </a>
        )}
      </div>
    </div>
  );
};

export default withStyles(messageHeaderStyleSheet)(MessageHeader);
