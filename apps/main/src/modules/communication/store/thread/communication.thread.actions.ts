import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { THREAD_FETCH } from './communication.thread.constants';

const fetchThreadAjaxAction = createAjaxAction(THREAD_FETCH);

/**
 * @param {string} uuid
 * @return {Function}
 */
export const fetchThreadAction = (uuid: string) =>
  fetchThreadAjaxAction({
    url: `/api/v2/communication/get_message_list?thread_uuid=${uuid}`,
    method: 'GET',
  });
