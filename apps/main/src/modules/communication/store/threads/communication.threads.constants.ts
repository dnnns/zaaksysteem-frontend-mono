import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const THREADS_FETCH = createAjaxConstants('THREADS_FETCH');
