interface Messages {
  [key: string]: string;
}

type AddMessages = (items: Messages) => void;
type RemoveMessages = (items: Messages) => void;

let messages: Messages = {};

const addMessages: AddMessages = items => {
  messages = {
    ...messages,
    ...items,
  };
};

const removeMessages: RemoveMessages = items => {
  const removeKeys = Object.keys(items);

  messages = Object.entries(messages)
    .filter(([key]) => removeKeys.includes(key))
    .reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: value,
      }),
      {}
    );
};

export default function useMessages(): [Messages, AddMessages, RemoveMessages] {
  return [messages, addMessages, removeMessages];
}
