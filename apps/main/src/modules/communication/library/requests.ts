import { request } from '@zaaksysteem/common/src/library/fetch';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  APISearchContactResponseBody,
  APISearchCaseResponseBody,
} from '../../../_generated/Communication.types';

export const searchContact = (
  keyword: string
): Promise<APISearchContactResponseBody> =>
  request(
    'GET',
    buildUrl(`/api/v2/communication/search_contact`, {
      keyword,
    })
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));

export const searchCase = (
  searchTerm: string
): Promise<APISearchCaseResponseBody> =>
  request(
    'GET',
    buildUrl(`/api/v2/communication/search_case`, {
      search_term: searchTerm,
      minimum_permission: 'read',
      limit: 25,
    })
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));
