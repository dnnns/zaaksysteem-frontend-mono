import React from 'react';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import { components } from 'react-select';

/**
 * @param {*} props
 * @return {ReactElement}
 */
const ClearIndicator = props => (
  <components.ClearIndicator {...props}>
    <CloseIndicator />
  </components.ClearIndicator>
);

export default ClearIndicator;
