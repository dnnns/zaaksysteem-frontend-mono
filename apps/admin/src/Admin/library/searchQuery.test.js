import { queryToObject, objectToQuery } from './searchQuery';

describe('The `searchQuery` module', () => {
  describe('exports a objectToQuery function that', () => {
    describe('transforms a search query to a complex object', () => {
      test('It handles generated queries correctly', () => {
        const obj = {
          foo: 'bar',
          array: ['foo', 'bar'],
          bar: 'value with whitespace',
          keyword: 'string with whitespace',
        };
        const query = objectToQuery(obj);

        expect(obj).toEqual(queryToObject(query));
      });
    });
  });
});
