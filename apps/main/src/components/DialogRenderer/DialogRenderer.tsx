import React from 'react';
import { connect } from 'react-redux';
import { hideDialog, DialogActionPayload } from '../../store/ui/ui.actions';
import useDialogs from '../../library/useDialogs';
import { State } from '../../store/interface';
import { Dispatch } from 'redux';

interface PropsFromState {
  dialogs: DialogActionPayload[];
}

interface PropsFromDispatch {
  closeAction: (dialogType: string) => void;
}

const DialogRenderer: React.FunctionComponent<
  PropsFromState & PropsFromDispatch
> = ({ dialogs, closeAction }) => {
  const [availableDialogs] = useDialogs();

  return (
    <React.Fragment>
      {dialogs
        .map<[DialogActionPayload, Function | undefined]>(dialog => [
          dialog,
          availableDialogs[dialog.dialogType],
        ])
        .map(([dialog, Component]) => {
          if (dialog && Component) {
            const { dialogType, ...restProps } = dialog;
            const closeDialog = () => closeAction(dialogType);

            return (
              <Component
                key={dialogType}
                onClose={closeDialog}
                {...restProps}
              />
            );
          }
        })}
    </React.Fragment>
  );
};

const mapStateToProps = ({ ui: { dialogs } }: State): PropsFromState => ({
  dialogs,
});

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => ({
  closeAction: (dialogType: string) => dispatch(hideDialog(dialogType)),
});

export default connect<PropsFromState, PropsFromDispatch, {}, State>(
  mapStateToProps,
  mapDispatchToProps
)(DialogRenderer);
