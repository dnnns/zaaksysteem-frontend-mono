import React from 'react';
import { useTranslation } from 'react-i18next';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';

import ThreadListItem from './ThreadListItem';

const NoteThreadListItem = ({ createdByName, ...rest }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.note.title', {
    createdByName,
  });

  return (
    <ThreadListItem
      {...rest}
      title={title}
      {...addScopeProp('thread', 'note')}
      icon={<ZsIcon size="medium">threadType.note</ZsIcon>}
    />
  );
};

export default NoteThreadListItem;
