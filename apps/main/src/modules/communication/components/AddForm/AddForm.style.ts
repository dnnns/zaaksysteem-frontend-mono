import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const addFormStyleSheet: StyleRulesCallback<any, {}> = ({
  typography,
  palette: { primary, error },
  mintlab: { greyscale, shadows, radius },
}: any) => ({
  addFormWrapper: {
    maxWidth: '700px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '14px',
    width: '100%',
  },
  tabsRoot: {
    marginBottom: '40px',
  },
  tabWrapper: {
    flexDirection: 'row',
    backgroundColor: primary.light,
    color: primary.main,
    borderRadius: '18px',
    padding: '6px',
  },
  tabLabelContainer: {
    padding: '0px',
  },
  tabIndicator: {
    display: 'none',
  },
  tabLabel: {
    color: primary.main,
  },
  colLabels: {
    width: '100px',
  },
  formWrapper: {
    width: '100%',
    border: `1px solid ${greyscale.dark}`,
    borderRadius: '10px',
    boxShadow: shadows.flat,
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: '16px',
    '&>:first-child': {
      marginRight: '16px',
    },
  },
  formRow: {
    padding: '24px',
    borderBottom: `1px solid ${greyscale.dark}`,
  },
  formRowBackground: {
    '&>:first-child': {
      backgroundColor: greyscale.light,
      borderRadius: radius.defaultFormElement,
    },
  },
  error: {
    ...typography.caption,
    color: error.main,
    marginTop: '8px',
    borderRadius: '0px',
  },
});
