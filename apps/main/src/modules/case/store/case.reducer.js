import { CASE_FETCH } from './case.constants';
import { AJAX_STATE_INIT } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import handleAjaxStateChange from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';

const initialState = {
  id: null,
  requestor: null,
  state: AJAX_STATE_INIT,
};

const normalizeCaseData = response => {
  const { requestor, id } = response.result.instance;
  const {
    reference,
    instance: { date_created, display_name, subject_type },
  } = requestor;

  return {
    id,
    requestor: {
      date_created,
      display_name,
      subject_type,
      reference,
    },
  };
};

const handleCaseStateChange = handleAjaxStateChange(CASE_FETCH);

export const caseReducer = (state = initialState, action) => {
  switch (action.type) {
    case CASE_FETCH.PENDING:
    case CASE_FETCH.ERROR:
      return {
        ...state,
        ...handleCaseStateChange(state, action),
      };

    case CASE_FETCH.SUCCESS:
      return {
        ...handleCaseStateChange(state, action),
        ...normalizeCaseData(action.payload.response),
      };

    default:
      return state;
  }
};
