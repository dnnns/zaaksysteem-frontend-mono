import React, { useEffect } from 'react';
import { withStyles } from '@material-ui/styles';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { Body1 } from '@mintlab/ui/App/Material/Typography';
import { useTranslation } from 'react-i18next';
import SwitchViewButton from '../SwitchViewButton/SwitchViewButton';
import { threadStyleSheet } from './Thread.style';
import ContactMomentMessage from './Message/ContactMomentMessage';
import NoteMessage from './Message/NoteMessage';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../library/communicationTypes.constants';
import ContactMomentThreadTitle from './ThreadTitle/ContactMomentThreadTitle';
import NoteThreadTitle from './ThreadTitle/NoteThreadTitle';
import PlusButtonSpaceWrapper from '../../../../components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
import ErrorBoundary from '../../../../components/ErrorBoundary/ErrorBoundary';
import { AnyMessageType } from '../../types/Message.types';

const messageElement = {
  [TYPE_CONTACT_MOMENT]: ContactMomentMessage,
  [TYPE_NOTE]: NoteMessage,
};

const titleElement = {
  [TYPE_CONTACT_MOMENT]: ContactMomentThreadTitle,
  [TYPE_NOTE]: NoteThreadTitle,
};

export interface ThreadPropsType {
  classes: any;
  fetchThread: (threadId: string) => void;
  messages: AnyMessageType[];
  match: {
    params: {
      threadId: string;
    };
  };
  rootPath: string;
  width: string;
  busy: boolean;
  caseRelation?: {
    id: string;
    caseNumber: string;
  };
}

const Thread: React.FunctionComponent<ThreadPropsType> = ({
  classes,
  fetchThread,
  messages,
  match,
  rootPath,
  width,
  caseRelation,
  busy,
}) => {
  const [t] = useTranslation('communication');
  const threadId = match.params.threadId;

  useEffect(() => {
    fetchThread(threadId);
  }, [threadId]);

  if (!messages || busy) {
    return <Loader />;
  }

  if (messages.length === 0) {
    return <Body1>{t('thread.noContent')}</Body1>;
  }

  const [firstMessage] = messages;
  const TitleComponent = titleElement[firstMessage.type];

  return (
    <ErrorBoundary>
      <SwitchViewButton width={width} to={rootPath} />
      <div className={classes.threadWrapper}>
        <PlusButtonSpaceWrapper>
          <TitleComponent
            classes={{ root: classes.threadTitle }}
            message={firstMessage as any}
          />
          {messages.map(message => {
            const MessageComponent = messageElement[message.type];

            return (
              <MessageComponent
                key={message.id}
                message={message as any}
                caseNumber={caseRelation && caseRelation.caseNumber}
              />
            );
          })}
        </PlusButtonSpaceWrapper>
      </div>
    </ErrorBoundary>
  );
};

export default withStyles(threadStyleSheet)(Thread);
