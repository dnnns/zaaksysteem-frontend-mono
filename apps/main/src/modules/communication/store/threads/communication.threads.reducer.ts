import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { THREADS_FETCH } from './communication.threads.constants';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../library/communicationTypes.constants';
import {
  mapNoteThread,
  mapContactMomentThread,
} from './library/threadMapFunctions';
import { Reducer } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APIThreadListResponseBody } from '../../../../_generated/Communication.types';
import { AnyThreadType } from '../../types/Thread.types';

export interface CommunicationThreadsState {
  state: AjaxState;
  threads: AnyThreadType[];
}

const initialState: CommunicationThreadsState = {
  state: AJAX_STATE_INIT,
  threads: [],
};

const threadMappers = {
  [TYPE_NOTE]: mapNoteThread,
  [TYPE_CONTACT_MOMENT]: mapContactMomentThread,
};

const handleFetchSuccess = (
  state: CommunicationThreadsState,
  action: AjaxAction<APIThreadListResponseBody>
): CommunicationThreadsState => {
  const { response } = action.payload;

  if (!response.data) {
    return state;
  }

  return {
    ...state,
    threads: response.data.reduce<AnyThreadType[]>((acc, thread) => {
      const threadType = thread.attributes.thread_type;

      if (
        (threadType && threadType === TYPE_NOTE) ||
        threadType === TYPE_CONTACT_MOMENT
      ) {
        return [...acc, threadMappers[threadType](thread)];
      }

      return acc;
    }, []),
  };
};

export const threads: Reducer<CommunicationThreadsState, AjaxAction> = (
  state = initialState,
  action
) => {
  const { type } = action;
  const handleFetchThreadsStateChange = handleAjaxStateChange(THREADS_FETCH);

  switch (type) {
    case THREADS_FETCH.PENDING:
    case THREADS_FETCH.ERROR:
      return handleFetchThreadsStateChange(state, action);
    case THREADS_FETCH.SUCCESS:
      return handleFetchThreadsStateChange(
        handleFetchSuccess(state, action as AjaxAction<
          APIThreadListResponseBody
        >),
        action
      );
    default:
      return state;
  }
};

export default threads;
