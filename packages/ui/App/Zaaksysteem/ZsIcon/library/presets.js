// TO-DO get backgroundColors from theme, once the grid palette is implemented
const getPresets = ({ palette: { primary, secondary } }) => ({
  channel: {
    frontdesk: {
      name: 'room_service',
      backgroundColor: '#EFC16A',
    },
    assignee: {
      name: 'person',
      backgroundColor: '#FF0000',
    },
    email: {
      name: 'alternate_email',
      backgroundColor: '#B2749F',
    },
    mail: {
      name: 'drafts',
      backgroundColor: '#6635C1',
    },
    phone: {
      name: 'phone',
      backgroundColor: '#23BA86',
    },
    social_media: {
      name: 'thumb_up',
      backgroundColor: '#3B5998',
    },
    webform: {
      name: 'format_list_bulleted',
      backgroundColor: '#337FCB',
    },
  },
  threadType: {
    contactMoment: {
      name: 'chat_bubble',
      backgroundColor: 'green',
    },
    email: {
      name: 'email',
      backgroundColor: '#58C9BA',
    },
    note: {
      name: 'notes',
      backgroundColor: '#EBC32F',
      color: '#000000',
    },
  },
  entityType: {
    person: {
      name: 'people',
      backgroundColor: secondary.main,
    },
    company: {
      name: 'domain',
      backgroundColor: primary.dark,
    },
  },
});

export default getPresets;
