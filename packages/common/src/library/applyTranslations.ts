import { TFunction } from 'i18next';

export const applyTranslations = (
  inputObj: { [key: string]: any },
  t: TFunction
) => {
  function traverse(current: string | any[] | null | { [key: string]: any }) {
    if (!current) {
      return current;
    }

    if (typeof current === 'string') {
      return t(current);
    }

    if (Array.isArray(current)) {
      return current.map(traverse);
    }

    if (typeof current === 'object') {
      return Object.entries(current).reduce(
        (acc, [key, value]) => ({
          ...acc,
          [key]: traverse(value),
        }),
        {}
      );
    }

    return current;
  }
  return traverse(inputObj);
};
