import * as fieldTypes from '../View/Shared/Form/Constants/fieldTypes';

const formDefinition = [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'changeOnlineStatus:fields.reason.label',
    placeholder: 'changeOnlineStatus:fields.reason.placeholder',
  },
];

export default formDefinition;
