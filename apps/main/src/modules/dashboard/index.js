import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getDashboardModule } from './store/dashboard.module';
import DashboardContainer from './components/DashboardContainer';

export default function DashboardModule({ userId }) {
  return (
    <DynamicModuleLoader modules={[getDashboardModule(userId)]}>
      <DashboardContainer />
    </DynamicModuleLoader>
  );
}
