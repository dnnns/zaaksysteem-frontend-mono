import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const switchViewButtonStyleSheet: StyleRulesCallback<any, {}> = ({
  mintlab: { shadows, greyscale },
}: any) => ({
  root: {
    position: 'absolute',
    top: '10px',
    left: '10px',
  },
  label: {
    color: greyscale.darkest,
    backgroundColor: greyscale.dark,
    boxShadow: shadows.medium,
    borderRadius: '50%',
    '& svg': {
      transform: 'scale(0.65)',
    },
  },
});

export default switchViewButtonStyleSheet;
