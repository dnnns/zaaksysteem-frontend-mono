/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const fileStylesheet = ({ mintlab: { greyscale }, typography }) => ({
  card: {
    position: 'relative',
    padding: 4,
    height: 42,
    width: '100%',
  },
  flexContainer: {
    position: 'relative',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 1,
  },
  uploadProgress: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 3,
    right: 0,
    zIndex: 0,
  },
  name: {
    flex: '1 0 auto',
    ...typography.body1,
    lineHeight: 1,
  },
  iconContainer: {
    paddingLeft: 8,
    width: 50,
    color: greyscale.darker,
  },
  loader: {
    margin: 0,
    width: 31,
    height: 31,
  },
});
