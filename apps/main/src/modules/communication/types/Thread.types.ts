export interface ThreadType {
  id: string;
  type: string;
  summary: string;
  createdByName: string;
  date: Date;
  unread: boolean;
  tag: string;
}

export interface ContactMomentThreadType extends ThreadType {
  direction: string;
  channel: string;
  withName: string;
}

export type AnyThreadType = ThreadType | ContactMomentThreadType;
