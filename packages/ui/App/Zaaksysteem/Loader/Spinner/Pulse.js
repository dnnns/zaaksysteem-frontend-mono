import React from 'react';
import style from './Pulse.module.css';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Pulse = ({ color, scope }) => (
  <div
    className={style['pulse']}
    style={{
      backgroundColor: color,
    }}
    {...addScopeAttribute(scope)}
  />
);
