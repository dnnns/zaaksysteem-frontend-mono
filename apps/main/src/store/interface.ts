import { RouterState } from 'connected-react-router';
import { UIState } from './ui/ui.reducer';

export interface State {
  ui: UIState;
  router: RouterState;
}
