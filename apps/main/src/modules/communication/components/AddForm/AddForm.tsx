import React from 'react';
import { withStyles } from '@material-ui/styles';
import ContactMomentContainer from './ContactMoment/ContactMomentContainer';
import NoteContainer from './Note/NoteContainer';
import { addFormStyleSheet } from './AddForm.style';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../library/communicationTypes.constants';
import Tabs from './Tabs/Tabs';
import SwitchViewButton from '../SwitchViewButton/SwitchViewButton';
import PlusButtonSpaceWrapper from '../../../../components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
import ErrorBoundary from '../../../../components/ErrorBoundary/ErrorBoundary';
import { TypeOfMessage } from '../../types/Message.types';

const components = {
  [TYPE_CONTACT_MOMENT]: ContactMomentContainer,
  [TYPE_NOTE]: NoteContainer,
};

type AddFormPropsType = {
  match: {
    params: {
      type: TypeOfMessage;
    };
  };
  classes: any;
  width: string;
  rootPath: string;
};

const AddForm: React.FunctionComponent<AddFormPropsType> = ({
  match,
  classes,
  width,
  rootPath,
}) => {
  const type = match.params.type;
  const ContainerElement = components[type];

  return (
    <div className={classes.addFormWrapper}>
      <SwitchViewButton width={width} to={rootPath} />
      <Tabs rootPath={rootPath} value={type} />
      <ErrorBoundary>
        <PlusButtonSpaceWrapper>
          <ContainerElement />
        </PlusButtonSpaceWrapper>
      </ErrorBoundary>
    </div>
  );
};

export default withStyles(addFormStyleSheet)(AddForm);
