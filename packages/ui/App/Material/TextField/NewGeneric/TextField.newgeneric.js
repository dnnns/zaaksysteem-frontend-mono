import React, { Component, createElement } from 'react';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/styles';
import { callOrNothingAtAll, unique } from '@mintlab/kitchen-sink/source';
import { textFieldStyleSheet } from './TextField.newgeneric.style';
import classNames from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import { bind } from '@mintlab/kitchen-sink/source';
import { addScopeAttribute } from '../../../library/addScope';

/**
 *  *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * This is a simple TextField with generic styling,
 * meant to be used as a general GUI component.
 *
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {string} placeholder
 * @reactProps {string} name
 * @reactProps {ReactElement} [startAdornment]
 * @reactProps {ReactElement} [endAdornment]
 * @reactProps {Function} [closeAction]
 * @reactProps {string} value
 * @reactProps {function} onChange
 * @reactProps {function} onKeyPress
 * @reactProps {function} onBlur
 * @reactProps {string} [scope]
 * @reactProps {boolean} [autoFocus]
 * @reactProps {string} [scope]
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
    };
    bind(this, 'EndAdornment', 'StartAdornment', 'handleFocus', 'handleBlur');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   * @return {ReactElement}
   */
  /* eslint-disable jsx-a11y/no-autofocus */
  render() {
    const {
      props: {
        classes,
        name,
        value,
        placeholder,
        onChange,
        onKeyPress,
        scope,
        inputProps,
        autoFocus,
      },
      state: { focus },
      StartAdornment,
      EndAdornment,
    } = this;

    return (
      <MuiTextField
        value={value}
        id={unique()}
        name={name}
        placeholder={placeholder}
        classes={{
          root: classNames(classes.formControl, {
            [classes.focus]: focus,
            [classes.rootFocus]: focus,
          }),
        }}
        InputProps={{
          classes: {
            input: classNames(classes.input, { [classes.focus]: focus }),
          },
          disableUnderline: true,
          ...StartAdornment(),
          ...EndAdornment(),
          ...inputProps,
        }}
        inputProps={{
          ...addScopeAttribute(scope, 'text-field-input'),
        }}
        onChange={onChange}
        onKeyPress={onKeyPress}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        autoFocus={autoFocus}
        {...addScopeAttribute(scope, 'text-field')}
      />
    );
  }

  handleFocus() {
    this.setState({ focus: true });
  }

  handleBlur(event) {
    const { onBlur } = this.props;
    this.setState({ focus: false });
    callOrNothingAtAll(onBlur, [event]);
  }

  /**
   * @return {ReactElement}
   */
  StartAdornment() {
    const {
      props: { classes, startAdornment },
      state: { focus },
    } = this;

    const props = {
      position: 'start',
      classes: {
        root: classNames({ [classes.focus]: focus }),
      },
    };

    return {
      startAdornment: createElement(InputAdornment, props, startAdornment),
    };
  }

  /**
   * @return {ReactElement}
   */
  EndAdornment() {
    const {
      props: { endAdornment, closeAction: action, classes, scope },
      state: { focus },
    } = this;

    const closeButton = createElement(CloseIndicator, { action, scope });
    const props = {
      position: 'end',
      classes: {
        root: classNames({ [classes.focus]: focus }, classes.endAdornment),
      },
    };

    if (endAdornment) {
      return {
        endAdornment: createElement(InputAdornment, props, endAdornment),
      };
    }

    if (action) {
      return {
        endAdornment: createElement(InputAdornment, props, closeButton),
      };
    }

    return null;
  }
}

export default withStyles(textFieldStyleSheet)(TextField);
