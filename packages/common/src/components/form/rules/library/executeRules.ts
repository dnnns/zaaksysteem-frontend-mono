import Rule from './Rule';
import { FormDefinitionField } from '../../types/formDefinition.types';

export const executeRules = (
  rules: Rule[],
  fields: FormDefinitionField[]
): FormDefinitionField[] => {
  return rules.reduce((accumulator, rule) => rule.execute(accumulator), fields);
};

export default executeRules;
