import React from 'react';
import Icon from '../../../Material/Icon';
import { withStyles } from '@material-ui/styles';
import { infoIconStyleSheet } from './InfoIcon.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const InfoIcon = ({ classes }) => (
  <Icon classes={classes}>help_outline</Icon>
);

export default withStyles(infoIconStyleSheet)(InfoIcon);
