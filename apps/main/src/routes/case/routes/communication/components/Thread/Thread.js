import React, { useEffect } from 'react';
import { withStyles } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { H3 } from '@mintlab/ui/App/Material/Typography';
import SwitchViewButton from '../SwitchViewButton/SwitchViewButton';
import { threadStyleSheet } from './Thread.style';
import ContactMomentMessage from './Message/ContactMomentMessage';
import NoteMessage from './Message/NoteMessage';

const messageElement = {
  contactmoment: ContactMomentMessage,
  note: NoteMessage,
};

const Thread = ({
  classes,
  fetchThread,
  type,
  messages,
  match,
  rootPath,
  width,
}) => {
  const [t] = useTranslation('communication');
  const threadId = match.params.threadId;

  useEffect(() => {
    fetchThread(threadId);
  }, [threadId]);

  if (!type) {
    return <Loader />;
  }

  const MessageElement = messageElement[type];

  return (
    <div className={classes.threadWrapper}>
      <SwitchViewButton width={width} to={rootPath} />
      <H3 classes={{ root: classes.threadTitle }}>
        {t(`threadTypes.${type}`)}
      </H3>
      {messages.map((message, index) => (
        <MessageElement key={index} message={message} />
      ))}
    </div>
  );
};

export default withStyles(threadStyleSheet)(Thread);
