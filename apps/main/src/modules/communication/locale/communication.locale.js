import * as CHANNELS from '../library/communicationChannels.constants';

export default {
  nl: {
    threadTypes: {
      contact_moment: 'Contactmoment',
      note: 'Notitie',
      mail: 'Bericht',
    },
    thread: {
      noContent: 'Er is geen communicatie om weer te geven',
      note: {
        messageTitle: 'Door {{user}}',
        title: 'Notitie van {{createdByName}}',
      },
      contactMoment: {
        messageTitle: '{{sender}} met {{receiver}}',
        title: 'Contactmoment via {{channel}}',
        subTitle: '{{createdByName}} met {{withName}}',
      },
      dateFormat: 'D MMM',
      searchPlaceholder: 'Zoeken',
      linkToCase: 'Ga naar zaak {{caseNumber}}',
    },
    placeholder: 'Selecteer een item om te lezen',
    direction: {
      incoming: 'Inkomend',
      outgoing: 'Uitgaand',
    },
    channels: {
      [CHANNELS.TYPE_CHANNEL_ASSIGNEE]: 'Behandelaar',
      [CHANNELS.TYPE_CHANNEL_FRONTDESK]: 'Balie',
      [CHANNELS.TYPE_CHANNEL_PHONE]: 'Telefoon',
      [CHANNELS.TYPE_CHANNEL_MAIL]: 'Post',
      [CHANNELS.TYPE_CHANNEL_EMAIL]: 'E-mail',
      [CHANNELS.TYPE_CHANNEL_WEBFORM]: 'Webformulier',
      [CHANNELS.TYPE_CHANNEL_SOCIALMEDIA]: 'Sociale media',
      [CHANNELS.TYPE_CHANNEL_EXTERNAL]: 'Externe applicatie',
    },
    addFields: {
      contact: 'Zoek een contact...',
      case: 'Zoek een zaak...',
      channel: 'Via',
      direction: 'Richting',
      content: 'Samenvatting contactmoment',
      note: 'Notitie',
    },
    serverErrors: {
      'communication/thread/not_created':
        'Er is iet mis gegaan bij het aanmaken.',
      'communication/contact/not_found': 'Onbekende verzender/ontvanger.',
      'communication/thread/cache/not_created':
        '$t(serverErrors.communication/thread/not_created)',
      'communication/message/type/not_created':
        '$t(serverErrors.communication/thread/not_created)',
      'communication/message/not_created':
        'Het bericht kon niet worden toegevoegd.',
    },
  },
};
