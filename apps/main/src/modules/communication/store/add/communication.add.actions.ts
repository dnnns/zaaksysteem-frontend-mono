import { ActionCreator } from 'redux';
//@ts-ignore
import uuidv4 from 'uuid/v4';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SAVE_COMMUNICATION } from './communication.add.constants';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../library/communicationTypes.constants';
import { CommunicationRootStateType } from '../communication.reducer';
import {
  APICreateContactMomentRequestBody,
  APICreateNoteRequestBody,
} from '../../../../_generated/Communication.types';

const saveAjaxAction = createAjaxAction(SAVE_COMMUNICATION);

/**
 * @param {Object} payload
 * @return {Function}
 */
export const saveAction: ActionCreator<ThunkActionWithPayload<
  CommunicationRootStateType,
  any
> | null> = payload => {
  const { type } = payload;

  switch (type) {
    case TYPE_CONTACT_MOMENT:
      return saveContactMoment(payload);
    case TYPE_NOTE:
      return saveNote(payload);
    default:
      return null;
  }
};

export type SaveContactMomentPayload = {
  values: Pick<
    APICreateContactMomentRequestBody,
    'content' | 'channel' | 'direction' | 'contactUuid' | 'caseUuid'
  >;
};

export const saveContactMoment = (
  payload: SaveContactMomentPayload
): ThunkActionWithPayload<
  CommunicationRootStateType,
  APICreateContactMomentRequestBody
> => dispatch => {
  const url = '/api/v2/communication/create_contact_moment';
  const { content, channel, direction, contactUuid, caseUuid } = payload.values;

  const data: APICreateContactMomentRequestBody = {
    thread_uuid: uuidv4(),
    contact_moment_uuid: uuidv4(),
    case_uuid: caseUuid || '',
    contact_uuid: contactUuid,
    channel,
    content,
    direction,
  };

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
    payload: data,
  })(dispatch);
};

type SaveNotePayload = {
  values: Pick<APICreateNoteRequestBody, 'content'>;
};

/**
 * @param {Object} payload
 * @return {Function}
 */
export const saveNote = (
  payload: SaveNotePayload
): ThunkActionWithPayload<
  CommunicationRootStateType,
  APICreateNoteRequestBody
> => (dispatch, getState) => {
  const {
    communication: { context },
  } = getState();

  const url = '/api/v2/communication/create_note';
  const { caseUuid, contactUuid } = context;
  const { content } = payload.values;

  const data: APICreateNoteRequestBody = {
    thread_uuid: uuidv4(),
    note_uuid: uuidv4(),
    content,
    ...(caseUuid === null
      ? { contact_uuid: contactUuid || '' }
      : { case_uuid: caseUuid }),
  };

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
    payload: data,
  })(dispatch);
};
