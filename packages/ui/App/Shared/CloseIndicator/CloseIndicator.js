import React from 'react';
import { withStyles } from '@material-ui/styles';
import { closeIndicatorStylesheet } from './CloseIndicator.style';
import Icon from '../../Material/Icon/Icon';
import IconButton from '@material-ui/core/IconButton';
import { addScopeAttribute } from '../../library/addScope';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} [props.action]
 * @return {ReactElement}
 */
const CloseIndicator = ({ classes, action, scope }) => (
  <IconButton
    onClick={action}
    color="inherit"
    classes={classes}
    {...addScopeAttribute(scope, 'close-button')}
  >
    <Icon size="extraSmall">close</Icon>
  </IconButton>
);

export default withStyles(closeIndicatorStylesheet)(CloseIndicator);
