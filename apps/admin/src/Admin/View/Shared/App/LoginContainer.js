import { connect } from 'react-redux';
import Login from './Login';
import { invoke } from '../../../Store/Route/route.actions';
import { login } from '../../../Store/Session/session.actions';
import { navigation } from '../../../Fixtures/navigation';

const mapStateToProps = ({
  app: { bootstrap },
  session: {
    data: { logged_in_user },
  },
}) => {
  return {
    capabilities: logged_in_user ? logged_in_user.capabilities : null,
    bootstrap,
  };
};

const mapDispatchToProps = dispatch => ({
  route(path) {
    dispatch(invoke(path));
  },
  login: payload => dispatch(login(payload)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  navigation,
});

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Login);

export default LoginContainer;
