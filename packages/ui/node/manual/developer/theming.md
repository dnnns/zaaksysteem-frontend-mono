# *Material-UI* theming

> *Material-UI* themes are fairly complex; since the
  style guide defines variations on standard concepts
  that are not supported by basic *Material-UI* 
  theming, we need a couple of creative workarounds.
  The most important thing to understand is exactly 
  *what* to change *where*, *how* and *why*.

## Colors

The *Material Design*
[color system](https://material.io/design/color/the-color-system.html#color-usage-palettes) 
features has a strong concept of `primary` and `secondary` colors,
each placed in a palette of lighter an darker variants.

Beyond that, generalized color classifications include 
*surface*, *background*, *error*, *typography* and *iconography*.

The *Material-UI* `theme` object reflects this in a `palette`
property with the three main uniform `PaletteIntention` object properties
`primary`, `secondary` and `error`, each of which with the properties
`light`, `main`, `dark` and `contrastText`.

An important aspect of theming is that you can pass these
`PaletteIntention` property names as a `prop` to components,
like `Button`, `Checkbox` and `Radio`. This automagically takes
care of secondary styling, like `hover` colors.

This imposes the following problems for our style guide:
- user defined `PaletteIntention`s like our `review`
  are ignored by the API
- user defined intention properties, like our 
  `darker` and `lighter` values for 
  `primary` and `secondary`
  are ignored by the API
- the API decides which intentions are useful in a given 
  context; for example, the `Button` component does 
  nothing with the `error` intention as value for its 
  `color` prop

Trying to reverse engineer these `color` prop effects 
manually would be tedious and might become brittle in 
the future. That's why we use a *nested theme* HoC in 
this case, mapping the main theme's palette `review` 
and `danger` intentions to `primary` and `secondary`
in the nested theme. It is important to understand that
this nests a **single component** in a separate theme,
so for performance reasons do not use this solution if 
there is another option.

## CSS API

Most *Component API*s have a distinct CSS API,
that defines keys that you can override with your own
style sheet. The common denominator is `root` for the 
root element.

### Global component style

The `theme` object contains an `override` object where you
can alter the default appearance of all components in
the theme. The `override` *key* is the component name 
prefixed with `Mui`. This is the preferred method of global
styling.

Example:

    const theme = {
      overrides: {
        MuiButton: {
          root: {
            textTransform: 'none',
          },
        },
      },
    };

### Local component style

In some cases you will need style that is associated
with the context of a *Material-UI* component. For
example, you might have two user defined components that 
use the same *Material-UI* component but it has to look 
substantially different.

You can achieve that with the `withStyles` HoC that passes
a style sheet as a `classes` prop to the contained component.

If you style sheet is a function, it is called with the 
theme object as argument.

Example style sheet:

    export const fooStyleSheet = theme => {
      root: {
        borderRadius: theme.shape.borderRadius,
      },
    };

Example usage:

    import Button from '@material-ui/core/Button';
    import { withStyles } from '@material-ui/styles';
    import { fooStyleSheet } from './foo.style.js';
    
    export const Foo = ({
      classes,
    }) => (
      <Button
        classes={classes}
      >{children}</Button
    );

    export default withStyles(fooStyleSheet)(Foo);

## Accessing the theme in a component

The `withTheme` HoC passes a `theme` prop to its contained
component.

    // pass `theme` as a prop
    export default withTheme(SomeComponent);

**Note**: if you use the `withStyles` HoC already, you
can just pass an options argument that sets the 
`withTheme` property to `true` in order to achieve
 exactly the same:

    // pass `classes` and `theme` as props
    export default withStyles(someStyleSheet, {
      withTheme: true,
    })(SomeComponent);

## *Material-UI* documentation

- Component API
    - [MuiThemeProvider](https://material-ui.com/api/mui-theme-provider/)
- Customization
    - [Themes](https://material-ui.com/customization/themes/)
    - [Default Theme](https://material-ui.com/customization/default-theme/)
- Style
    - [Color](https://material-ui.com/style/color/)

ZS-TODO: canonical URI reference for the style guide
