import { createAjaxAction } from '../../library/redux/ajax/createAjaxAction';
import {
  SESSION_FETCH,
  SESSION_AUTH_LOGIN,
  SESSION_AUTH_LOGOUT,
} from './session.constants';
import { ActionWithPayload } from '../../types/ActionWithPayload';

const fetchAjaxAction = createAjaxAction(SESSION_FETCH);

/**
 * @return {Function}
 */
export const fetchSession = () =>
  fetchAjaxAction({
    url: '/api/v1/session/current',
    method: 'GET',
  });

export const login = (payload: any): ActionWithPayload<any> => ({
  type: SESSION_AUTH_LOGIN,
  payload,
});

/**
 * @param {*} payload
 * @return {Function}
 */
export const logout = (payload: any): ActionWithPayload<any> => ({
  type: SESSION_AUTH_LOGOUT,
  payload,
});
