import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { THREADS_FETCH } from './communication.threads.constants';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationRootStateType } from '../communication.reducer';

const fetchThreadsAjaxAction = createAjaxAction(THREADS_FETCH);

/**
 * @return {Function}
 */
export const fetchThreadsAction = (): ThunkActionWithPayload<
  CommunicationRootStateType,
  {}
> => (dispatch, getState) => {
  const {
    communication: { context },
  } = getState();

  const url = buildUrl('/api/v2/communication/get_thread_list', {
    type: context.type,
    uuid: context.type === 'case' ? context.caseUuid : context.contactUuid,
  });

  return fetchThreadsAjaxAction({
    url,
    method: 'GET',
  })(dispatch);
};
