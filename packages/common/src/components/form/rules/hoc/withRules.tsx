import React, { ComponentType } from 'react';
import {
  getFormDefinitionAfterRules,
  getValuesDiff,
  getValuesFromDefinition,
  isFormDefinitionUpdated,
} from './helpers';
import {
  FormDefinitionField,
  FormDefinition,
} from '../../types/formDefinition.types';
import Rule from '../library/Rule';
import { FormProps } from '../../library/Form';

interface RulesEngineProps extends FormProps {
  formDefinition: FormDefinitionField[];
  rules: Rule[];
}

interface RulesEngineState {
  busy: boolean;
}

interface WithRulesOptions {
  Component: ComponentType<FormProps>;
  setFormDefinition(formDefinition: FormDefinition): void;
}

export const withRules = ({
  Component,
  setFormDefinition,
}: WithRulesOptions) => {
  return class RulesEngine extends React.Component<
    RulesEngineProps,
    RulesEngineState
  > {
    state = {
      busy: true,
    };

    componentDidMount() {
      this.update();
    }

    componentDidUpdate() {
      this.update();
    }

    render() {
      const { formDefinition, rules, ...rest } = this.props;

      if (this.state.busy) {
        return null;
      }
      return <Component {...rest} formDefinition={formDefinition} />;
    }

    update() {
      const { setValues, rules, formDefinition, values } = this.props;
      const updatedFormDefinition = getFormDefinitionAfterRules(
        rules,
        formDefinition,
        values
      );

      const updatedValues = getValuesFromDefinition(updatedFormDefinition);
      const valueDiff = getValuesDiff(values, updatedValues);
      const hasUpdatedValues = Object.keys(valueDiff).length;
      const hasUpdatedFromDefinition = isFormDefinitionUpdated(
        this.props.formDefinition,
        updatedFormDefinition,
        values
      );

      if (hasUpdatedValues) {
        setValues({
          ...values,
          ...valueDiff,
        });
      }

      if (hasUpdatedFromDefinition) {
        setFormDefinition(updatedFormDefinition);
      } else if (this.state.busy) {
        this.setState({ busy: false });
      }
    }
  };
};

export default withRules;
