import React from 'react';
import toJson from 'enzyme-to-json';
import { mount, shallow } from 'enzyme';
import { Breadcrumbs } from './Breadcrumbs';
import MaterialUiThemeProvider from '../MaterialUiThemeProvider/MaterialUiThemeProvider';

describe('The `Breadcrumbs` component', () => {
  const items = [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Level 1',
      path: '/level',
    },
    {
      label: 'Level 2',
      path: '/level/level',
    },
    {
      label: 'Level 3',
      path: '/level/level/level',
    },
    {
      label: 'Level 4',
      path: '/level/level/level/level',
    },
  ];

  test('renders correctly', () => {
    const component = shallow(
      <Breadcrumbs
        items={items}
        maxItems={4}
        onItemClick={() => {}}
        classes={{}}
      />
    );

    expect(toJson(component)).toMatchSnapshot();
  });

  describe('with custom renderer functions', () => {
    test('are executed', () => {
      const itemRenderer = jest.fn();
      const lastItemRenderer = jest.fn();

      mount(
        <MaterialUiThemeProvider>
          <Breadcrumbs
            items={items}
            itemRenderer={itemRenderer}
            lastItemRenderer={lastItemRenderer}
          />
        </MaterialUiThemeProvider>
      );

      expect(itemRenderer.mock.calls.length).toBe(4);
      expect(lastItemRenderer.mock.calls.length).toBe(1);
    });
  });
});
