import React, { ComponentType } from 'react';
import { FormikProps, FormikActions } from 'formik';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import {
  FormDefinition,
  FormRendererRenderProps,
  FormRendererFormField,
} from '../types/formDefinition.types';

export interface FormProps extends FormikProps<any> {
  formDefinition: FormDefinition;
  Fields: {
    [key: string]: ComponentType;
  };
  children: (computedProps: FormRendererRenderProps) => any;
}

const setTouchedAndHandleChange = ({
  setFieldTouched,
  handleChange,
}: Pick<FormikActions<any>, 'setFieldTouched'> &
  Pick<FormikProps<any>, 'handleChange'>) => (
  event: React.SyntheticEvent<HTMLInputElement>
) => {
  const {
    currentTarget: { name },
  } = event;
  setFieldTouched(name, true);
  handleChange(event);
};

export const Form: React.FunctionComponent<FormProps> = ({
  Fields,
  errors,
  children,
  touched,
  handleBlur,
  setFieldValue,
  setFieldTouched,
  handleChange,
  values,
  formDefinition,
  isValid,
  ...rest
}) => {
  const fields: FormRendererFormField[] = formDefinition
    .filter(field => !field.hidden)
    .map(field => {
      const { name } = field;

      return {
        ...field,
        definition: field,
        FieldComponent: Fields[field.type],
        value: values[name],
        checked: values[name],
        error: touched[name] ? errors[name] : undefined,
        touched: touched[name] ? true : false,
        key: `field-${name}`,
        refValue: get(values, `${field.refValue}`, null),
        onChange: setTouchedAndHandleChange({
          setFieldTouched,
          handleChange,
        }),
        onBlur: handleBlur,
        setFieldValue,
      };
    });

  return children({
    fields,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    handleChange,
    values,
    isValid,
    errors,
    touched,
    ...rest,
  });
};
