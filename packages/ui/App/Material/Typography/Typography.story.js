import { React, stories, text } from '../../story';
import {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
  Subtitle1,
  Subtitle2,
  Body1,
  Body2,
  Caption,
  Overline,
} from '.';

stories(module, __dirname, {
  Default() {
    const components = [
      { name: 'h1', component: H1 },
      { name: 'h2', component: H2 },
      { name: 'h3', component: H3 },
      { name: 'h4', component: H4 },
      { name: 'h5', component: H5 },
      { name: 'h6', component: H6 },
      { name: 'Subtitle1', component: Subtitle1 },
      { name: 'Subtitle2', component: Subtitle2 },
      { name: 'Body1', component: Body1 },
      { name: 'Body2', component: Body2 },
      { name: 'Caption', component: Caption },
      { name: 'Overline', component: Overline },
    ];
    const displayText = text(
      'Text',
      'The quick brown fox jumps over the lazy dog.'
    );

    return (
      <div>
        {components.map(item => (
          <div key={item.name} style={{ marginBottom: 20 }}>
            <div style={{ backgroundColor: '#eee', padding: 5 }}>
              <Body1>{item.name}:</Body1>
            </div>
            <div style={{ padding: 5 }}>
              <item.component>{displayText}</item.component>
            </div>
          </div>
        ))}
      </div>
    );
  },
});
