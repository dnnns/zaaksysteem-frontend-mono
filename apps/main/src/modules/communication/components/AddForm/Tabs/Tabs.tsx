import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import MUITabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import { tabsStyleSheet } from './Tabs.style';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../../library/communicationTypes.constants';

type TabsPropsType = {
  classes: any;
  value: string;
  rootPath: string;
};

const Tabs: React.FunctionComponent<TabsPropsType> = ({
  classes,
  value,
  rootPath,
}) => {
  const tabClasses = {
    root: classes.tabRoot,
    wrapper: classes.tabWrapper,
    selected: classes.selected,
  };

  type LinkComponentPropsType = {
    linktype: string;
  };

  const LinkComponent = React.forwardRef<
    HTMLAnchorElement,
    LinkComponentPropsType
  >((props, ref) => (
    <Link innerRef={ref} to={`${rootPath}/new/${props.linktype}`} {...props} />
  ));
  LinkComponent.displayName = 'tabLinkComponent';

  const [t] = useTranslation();

  return (
    <MUITabs
      classes={{
        root: classes.tabsRoot,
        flexContainer: classes.tabsFlexContainer,
      }}
      TabIndicatorProps={{
        className: classes.tabIndicator,
      }}
      value={value}
      variant="fullWidth"
      indicatorColor="primary"
    >
      <Tab
        label={t('communication:threadTypes.contact_moment')}
        value={TYPE_CONTACT_MOMENT}
        linktype={TYPE_CONTACT_MOMENT}
        classes={{ ...tabClasses }}
        icon={<Icon size="small">chat_bubble</Icon>}
        disableTouchRipple={true}
        component={LinkComponent}
        {...addScopeAttribute('add-form', 'tab', TYPE_CONTACT_MOMENT)}
      />
      <Tab
        label={t('communication:threadTypes.note')}
        value={TYPE_NOTE}
        linktype={TYPE_NOTE}
        icon={<Icon size="small">sort</Icon>}
        classes={tabClasses}
        disableTouchRipple={true}
        component={LinkComponent}
        {...addScopeAttribute('add-form', 'tab', TYPE_CONTACT_MOMENT)}
      />
    </MUITabs>
  );
};

export default withStyles(tabsStyleSheet)(Tabs);
