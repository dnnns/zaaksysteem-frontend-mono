//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { AJAX_STATE_INIT } from '../../library/redux/ajax/createAjaxConstants';
import { SESSION_FETCH } from './session.constants';
import {
  handleAjaxStateChange,
  WithAjaxState,
} from '../../library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '../../library/redux/ajax/createAjaxAction';
import { Action } from 'redux';

type SessionData = {};

export interface SessionState extends WithAjaxState {
  data: {} | SessionData;
}

const initialState: SessionState = {
  data: {},
  state: AJAX_STATE_INIT,
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const fetchSuccess = (state: SessionState, action: AjaxAction) => {
  const data = get(action, 'payload.response.result.instance');
  return {
    ...state,
    data,
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function session(state: SessionState = initialState, action: Action) {
  const handleAjaxState = handleAjaxStateChange(SESSION_FETCH);

  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return fetchSuccess(
        handleAjaxState<SessionState>(state, action as AjaxAction),
        action as AjaxAction
      );
    default:
      return state;
  }
}
