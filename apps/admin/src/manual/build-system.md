# Build System

> The build system code under `node/bin` is responsible for creating
  the webpack vendor and app bundles and all its assets and generating
  the SPA entry point HTML file for the web server.

## Vendor bundles

Vendor assets consist of third party code that is installed with a 
package manager. They are built separately as webpack DLL bundles.

The build executable is `./bin/vendor`. It also includes the 
specific webpack configurations for the vendor bundle builds.

### `ie11`

The `ie11` bundle is conditionally loaded and just monkey-patches 
the browser's host environment with ECMAScript and DOM polyfills 
that are only needed for *Internet Explorer 11*.

NB: it *would* match outdated versions of evergreen browsers with
unknown results; those are simply not supported. 

See `node/bin/library/template/script-loader.js` for details.

### `react`

The `react` bundle provides *React* itself and other libraries 
from its ecosystem.

Its DLL manifest is consumed by the *ui* bundle and the 
*Admin* app bundle.

### `ui`

The `ui` bundle provides Mintlab React UI components largely
based on *Material Design*. It also acts as a facade for the
chosen *Material Design* library implementation.

Its DLL manifest is consumed by the *Admin* app bundle.

## App bundles

There is currently only one *app*, `Admin`, and one 
[main development stack](./app.html#main-development-stack),
`react:redux`. Adding another app with the same 
*main development stack* is simply a matter of following
the steps documented in 
[Apps](./app.html).

Note that adding a new *main development stack() needs to be 
implemented in the build executable. In the simplest case, 
you'd just need to build other vendor assets and pass them to 
the HTML entry point generator. 
In the most complex case, you might need an entirely 
different webpack configuration, or even a different bundler 
altogether.

**Managing** *main development stack* dependencies, however, 
is already supported with `zs add` and `zs remove` because that 
was easy to do without knowing the build context itself. 
See [packages](./packages.html) for more information.

The build executable is `./bin/build`. It builds **all** apps 
and depends on `./bin/vendor` having been run first.

NB: In the development container, this command is also 
avaiable as `zs build`, including an integrity check
for the vendor DLL manifests.
This is useful if you need to test a static build in 
your own environment. 

## HTML

The SPA entry point file for the web server is generated with
`node/bin/library/template.js` using the assets in 
`node/bin/library/template`.

It provides low level error handling and a script loader that
conditionally includes the IE 11 polyfill bundle with duck typing,
fetches resources in parallel and executes the script resources
in their respective request order.
