import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { compose } from 'recompose';
import { withTranslation } from 'react-i18next';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import Note, { NotePropsType } from './Note';
import { saveAction } from '../../../store/add/communication.add.actions';
import formDefinition from '../fixtures/note';
import { CommunicationRootStateType } from '../../../store/communication.reducer';

type PropsFromStateType = Pick<
  NotePropsType,
  'rootPath' | 'busy' | 'formDefinition'
>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
      context: { rootPath },
    },
  } = stateProps;

  return {
    rootPath,
    busy: state === AJAX_STATE_PENDING,
    formDefinition,
  };
};

type PropsFromDispatchType = Pick<NotePropsType, 'save'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => {
  return {
    save(payload: { [key: string]: any }) {
      const action = saveAction(payload);
      if (action !== null) {
        dispatch(action as any);
      }
    },
  };
};

const connected = compose<PropsFromStateType & PropsFromDispatchType, {}>(
  withTranslation(),
  connect<
    PropsFromStateType,
    PropsFromDispatchType,
    {},
    CommunicationRootStateType
  >(
    mapStateToProps,
    mapDispatchToProps
  )
)(Note);

export default connected;
