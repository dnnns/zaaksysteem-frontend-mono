import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { saveFolder } from '../../../../Store/Catalog/Folder/folder.actions';
import Folder from './Folder';
import uuidv4 from 'uuid/v4';
import formDefinition from '../../../../Fixtures/folder';

const mangleValuesForSave = ({ name, id, currentFolderUUID }) => {
  const isNew = !id;
  const folder_uuid = isNew ? uuidv4() : id;

  return {
    name,
    folder_uuid,
    parent_uuid: currentFolderUUID,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      folder,
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const mapFormDefinition = field => {
    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      loadingMessage: t(field.loadingMessage),
      value: folder.name,
    };
  };
  const newProps = {
    ...folder,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
  };

  return {
    busy: folder.savingState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  dispatchSave: params => dispatch(saveFolder(params)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentFolderUUID, id } = stateProps;
  const { dispatchSave } = dispatchProps;

  return {
    ...stateProps,
    ...ownProps,
    saveAction: ({ values }) =>
      dispatchSave(
        mangleValuesForSave({
          name: values.name,
          id,
          currentFolderUUID,
        })
      ),
  };
};

const connectedDialog = translate()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(Folder)
);

export default connectedDialog;
