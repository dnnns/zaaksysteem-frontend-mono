import React, { Component } from 'react';
import { translate } from 'react-i18next';
import fecha from 'fecha';

/**
 * FormatDate provider. Customizes the fecha instance
 * with translations from the locale and provides the
 * `format` function as the value.
 *
 * Do *not* use these directly -- use the withFormatDate HOC
 * to wrap a component to have it receive the formatDate
 * function.
 */

export const FormatDateContext = React.createContext();

/**
 * @reactProps {Object} props
 * @reactProps {Function} props.t
 * @return {ReactElement}
 */
class FormatDateProvider extends Component {
  constructor(props) {
    super(props);

    const { t } = props;

    fecha.i18n = {
      ...fecha.i18n,
      ...t('common:dates', { returnObjects: true }),
    };
  }

  render() {
    return (
      <FormatDateContext.Provider value={fecha.format}>
        {this.props.children}
      </FormatDateContext.Provider>
    );
  }
}

const providerWithTranslations = translate()(FormatDateProvider);

export default providerWithTranslations;
