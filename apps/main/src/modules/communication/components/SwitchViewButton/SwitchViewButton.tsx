import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/styles';
//@ts-ignore
import { Button } from '@mintlab/ui/App/Material/Button';
import switchViewButtonStyleSheet from './SwitchViewButton.style';

type SwitchViewButtonPropsType = {
  classes: any;
  to: string;
  width: string;
};

const SwitchViewButton: React.FunctionComponent<SwitchViewButtonPropsType> = ({
  classes,
  to,
  width,
}) => {
  const small = ['xs', 'sm'].includes(width);
  const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
    <Link to={to} innerRef={ref} {...props} />
  ));
  LinkComponent.displayName = 'switchViewButton';

  if (!small) return null;

  return (
    <Button
      component={LinkComponent}
      {...({ classes } as any)}
      presets={['icon', 'medium']}
    >
      arrow_back
    </Button>
  );
};

export default withStyles(switchViewButtonStyleSheet)(SwitchViewButton);
