import { RuleEngineCondition } from './conditions';
import { RuleEngineActionExecution } from './actions';
import {
  FormDefinitionField,
  FormValue,
} from '../../types/formDefinition.types';

const triggerActions = (
  actions: RuleEngineActionExecution[] | RuleEngineActionExecution,
  fields: FormDefinitionField[]
): FormDefinitionField[] =>
  Array.isArray(actions)
    ? actions.reduce((thisFields, action) => action(thisFields), fields)
    : actions(fields);

export class Rule {
  private ifFn: RuleEngineCondition | undefined;
  private thenFn: RuleEngineActionExecution[] = [];
  private elseFn: RuleEngineActionExecution[] = [];
  private activeAction: string | null = null;

  constructor(private fieldName: string) {}

  when(fn: RuleEngineCondition) {
    this.ifFn = fn;
    return this;
  }

  then(fn: RuleEngineActionExecution) {
    this.activeAction = 'then';
    this.thenFn.push(fn);
    return this;
  }

  else(fn: RuleEngineActionExecution) {
    this.activeAction = 'else';
    this.elseFn.push(fn);
    return this;
  }

  and(fn: RuleEngineActionExecution) {
    switch (this.activeAction) {
      case 'then':
        return this.then(fn);

      case 'else':
        return this.else(fn);

      default:
        throw new Error(
          'Cannot call and(...) before calling then(...) or when(...)'
        );
    }
  }

  execute(fields: FormDefinitionField[]) {
    const field = fields.find(item => item.name === this.fieldName);

    if (field && field.value) {
      if (this.ifFn && this.ifFn(field.value as FormValue)) {
        return triggerActions(this.thenFn, fields);
      } else if (this.elseFn) {
        return triggerActions(this.elseFn, fields);
      }
    }

    return fields;
  }
}

export default Rule;
