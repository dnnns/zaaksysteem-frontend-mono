const { join } = require('path');

module.exports = {
  chunkFilename: '[name].[chunkhash].js',
  filename: '[name].js',
  libraryTarget: 'commonjs2',
  path: join(process.cwd(), 'distribution'),
};
