import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import { get } from '@mintlab/kitchen-sink/source';
import getPresets from './library/presets';
import IconRounded from '../../Material/IconRounded';

/**
 * Zaaksysteem Icon component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Icon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Object} props.theme
 * @param {string} props.children
 * @return {ReactElement}
 */
export const ZsIcon = props => {
  const { theme, children } = props;
  const { name, color, backgroundColor } = get(getPresets(theme), children);
  const white = theme.palette.common.white;

  return (
    <IconRounded
      {...props}
      backgroundColor={backgroundColor}
      style={{ color: color || white, ...props.style }}
    >
      {name}
    </IconRounded>
  );
};

export default withTheme(ZsIcon);
