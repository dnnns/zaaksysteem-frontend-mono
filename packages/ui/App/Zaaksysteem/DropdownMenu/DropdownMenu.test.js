import React from 'react';
import toJson from 'enzyme-to-json';
import { DropdownMenu } from './DropdownMenu';
import { shallow } from 'enzyme';

/**
 * @test {DropdownMenu}
 */
describe('The `DropdownMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(
      <DropdownMenu items={items} classes={{}} manualId="testId">
        <b>Toggle</b>
      </DropdownMenu>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
