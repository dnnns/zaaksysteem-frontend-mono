import { caseReducer } from './case.reducer';
import { fetchCase } from './case.actions';

export function getCaseModule(caseIdFromRoute) {
  return {
    id: 'case',
    reducerMap: {
      case: caseReducer,
    },
    initialActions: [fetchCase(caseIdFromRoute)],
  };
}
