declare module '@mintlab/kitchen-sink/source' {
  function get<D, R, A = undefined>(
    data: D,
    valuePath: string,
    alternateValue?: A
  ): R | A;

  function buildUrl(url: string, params: { [key: string]: any }): string;

  function asArray(data: any): any[];
}
