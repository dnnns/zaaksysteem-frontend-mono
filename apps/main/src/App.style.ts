import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const appStyleSheet: StyleRulesCallback<any, {}> = ({ typography }) => ({
  app: {
    fontFamily: typography.fontFamily,
  },
});
