module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-typescript',
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-syntax-object-rest-spread',
    'babel-plugin-syntax-dynamic-import',
    'transform-class-properties',
  ],
  env: {
    test: {
      plugins: ['dynamic-import-node'],
    },
  },
};
