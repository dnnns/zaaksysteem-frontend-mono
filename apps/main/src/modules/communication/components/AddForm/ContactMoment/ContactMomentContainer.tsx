import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { compose } from 'recompose';
import { withTranslation } from 'react-i18next';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import ContactMoment, { ContactMomentProps } from './ContactMoment';
import { saveAction } from '../../../store/add/communication.add.actions';
//@ts-ignore
import contactMomentFormDefinition from '../fixtures/contactMoment';
import { CommunicationRootStateType } from '../../../store/communication.reducer';

type PropsFromState = Pick<
  ContactMomentProps,
  'rootPath' | 'formDefinition' | 'busy'
>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromState => {
  const {
    communication: {
      add: { state },
      context: { type, rootPath, contactUuid, caseUuid, contactName },
    },
  } = stateProps;

  const formDefinition = contactMomentFormDefinition.map(entry => {
    if (entry.name === 'contactUuid') {
      return type === 'case'
        ? {
            ...entry,
            choices: [
              {
                label: contactName,
                value: contactUuid,
              },
            ],
            value: contactUuid,
          }
        : {
            ...entry,
            value: contactUuid,
            hidden: true,
          };
    }

    if (entry.name === 'caseUuid') {
      return type === 'contact'
        ? entry
        : {
            ...entry,
            value: caseUuid,
            hidden: true,
          };
    }
    return entry;
  });

  return {
    rootPath,
    busy: state === AJAX_STATE_PENDING,
    formDefinition,
  };
};

type PropsFromDispatch = Pick<ContactMomentProps, 'save'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => {
  return {
    save(payload) {
      const action = saveAction(payload);
      if (action !== null) {
        dispatch(action as any);
      }
    },
  };
};

const connected = compose<PropsFromState & PropsFromDispatch, {}>(
  withTranslation(),
  connect<PropsFromState, PropsFromDispatch, {}, CommunicationRootStateType>(
    mapStateToProps,
    mapDispatchToProps
  )
)(ContactMoment);

export default connected;
