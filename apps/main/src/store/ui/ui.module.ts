import ui from './ui.reducer';

export const getUIModule = () => ({
  id: 'ui',
  reducerMap: {
    ui,
  },
});

export default getUIModule;
