import React from 'react';
import { H3 } from '@mintlab/ui/App/Material/Typography';

/**
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @return {ReactElement}
 */
export const Title = ({ titleStyle, children }) => (
  <H3
    classes={{
      root: titleStyle,
    }}
  >
    {children}
  </H3>
);

export default Title;
