import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICreateContactMomentResponseBody } from '../../../../_generated/Communication.types';
import { SAVE_COMMUNICATION } from './communication.add.constants';
import { SaveContactMomentPayload } from './communication.add.actions';

export interface CommunicationAddState {
  state: AjaxState;
}

const initialState: CommunicationAddState = {
  state: AJAX_STATE_INIT,
};

export const add: Reducer<
  CommunicationAddState,
  AjaxAction<APICreateContactMomentResponseBody, SaveContactMomentPayload>
> = (state = initialState, action) => {
  const { type } = action;

  const handleSaveAjaxStateChange = handleAjaxStateChange(SAVE_COMMUNICATION);

  switch (type) {
    case SAVE_COMMUNICATION.PENDING:
    case SAVE_COMMUNICATION.ERROR:
    case SAVE_COMMUNICATION.SUCCESS:
      return handleSaveAjaxStateChange(state, action);
    default:
      return state;
  }
};

export default add;
