/**
 * @param {Object} theme
 * @return {JSS}
 */
export const catalogHeaderStyleSheet = ({ mintlab: { greyscale } }) => ({
  headerWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    '&>*:not(:first-child):not(:last-child)': {
      marginRight: '10px',
    },
  },
  navigation: {
    display: 'flex',
  },
  center: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actions: {
    display: 'flex',
  },
  aap: {
    color: greyscale.darkest,
  },
});
