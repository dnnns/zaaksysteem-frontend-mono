import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import {
  TEXT,
  SELECT,
  CHECKBOX,
  FILE_SELECT,
  FLATVALUE_SELECT,
  EMAIL,
  TEXTAREA,
} from '../Constants/fieldTypes';
import FlatValueSelect from '../FlatValueSelect';
import Textarea from './Textarea';
import FileSelect from './FileSelect';

export const FormFields = {
  [TEXT]: TextField,
  [SELECT]: Select,
  [FLATVALUE_SELECT]: FlatValueSelect,
  [CHECKBOX]: Checkbox,
  [EMAIL]: TextField,
  [SELECT]: Select,
  [TEXT]: TextField,
  [TEXTAREA]: Textarea,
  [FILE_SELECT]: FileSelect,
};
