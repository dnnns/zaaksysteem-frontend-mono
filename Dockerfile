ARG CONTAINER_ROOT=/opt/zaaksysteem-frontend-mono

### --------- Stage: Setup NGINX for development ---------
FROM node:dubnium AS base

RUN set -x\
  && apt-get update\
  && apt-get install --no-install-recommends --no-install-suggests -y nginx

COPY nginx/entrypoint.sh /
COPY nginx/nginx-development.conf.tpl /etc/nginx/nginx.conf.tpl

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

### --------- Stage: Install Node.js root dependencies ---------
FROM base AS install-root-dependencies

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

ENV NODE_ENV="development"

# USER node
WORKDIR $CONTAINER_ROOT

COPY --chown=node:node package.json yarn.lock $CONTAINER_ROOT/

RUN yarn

### --------- Stage: Install Node.js project dependencies ---------
FROM install-root-dependencies as install-dependencies

WORKDIR $CONTAINER_ROOT

# Lerna
COPY --chown=node:node lerna.json $CONTAINER_ROOT/

# Apps
COPY --chown=node:node apps/main/package.json $CONTAINER_ROOT/apps/main/
COPY --chown=node:node apps/admin/package.json $CONTAINER_ROOT/apps/admin/

# Packages
COPY --chown=node:node packages/kitchen-sink/package.json $CONTAINER_ROOT/packages/kitchen-sink/
COPY --chown=node:node packages/ui/package.json $CONTAINER_ROOT/packages/ui/
COPY --chown=node:node packages/common/package.json $CONTAINER_ROOT/packages/common/

RUN yarn lerna:bootstrap

### --------- Stage: Development NGINX  ---------
### Proxy requests to Create React App devServer
FROM install-dependencies AS development

ARG WEBPACK_BUILD_TARGET
ENV WEBPACK_BUILD_TARGET $WEBPACK_BUILD_TARGET

COPY --chown=node:node tsconfig.json $CONTAINER_ROOT/tsconfig.json
COPY --chown=node:node apps $CONTAINER_ROOT/apps
COPY --chown=node:node packages $CONTAINER_ROOT/packages

### --------- Stage: Build production bundles  ---------
FROM development as production-build

WORKDIR $CONTAINER_ROOT

COPY --chown=node:node .eslintrc.js .prettierrc ./

# Set NODE_ENV after bootstrap because we need devDependencies
# to be installed to create a production build
ENV NODE_ENV="production"

RUN yarn lerna:build --scope "@zaaksysteem/main" --scope "@zaaksysteem/admin"

### --------- Stage: Production NGINX  ---------
FROM nginx:latest as production

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

COPY nginx/nginx.conf /etc/nginx/nginx.conf

# Apps build
COPY --from=production-build $CONTAINER_ROOT/apps/main/build/ /www/data/main
COPY --from=production-build $CONTAINER_ROOT/apps/admin/build/ /www/data/admin
