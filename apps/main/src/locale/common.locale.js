export default {
  nl: {
    dates: {
      dayNamesShort: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
      dayNames: [
        'Zondag',
        'Maandag',
        'Dinsdag',
        'Woensdag',
        'Donderdag',
        'Vrijdag',
        'Zaterdag',
      ],
      monthNamesShort: [
        'Jan',
        'Feb',
        'Mrt',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Okt',
        'Nov',
        'Dec',
      ],
      monthNames: [
        'Januari',
        'Februari',
        'Maart',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Augustus',
        'September',
        'Oktober',
        'November',
        'December',
      ],
      DoFn: month => month,
      dateFormatText: 'DD MMMM YYYY',
      dateFormat: 'DD-MM-YYYY',
      timeFormat: 'HH:mm',
      timeFormatFull: 'HH:mm:ss',
    },
    forms: {
      add: 'Toevoegen',
      ok: 'OK',
      cancel: 'Annuleren',
    },
    dialog: {
      error: {
        title: 'Fout',
        body:
          'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
      },
      ok: 'Ok',
    },
    serverErrors: {
      status: {
        '401': 'U bent niet geauthoriseerd om deze actie uit te voeren.',
        '403': 'U heeft niet genoeg rechten om deze actie uit te voeren.',
        '404':
          'De opgevraagde pagina of gegevensbron kon niet worden gevonden. Probeer het later opnieuw.',
      },
    },
    clientErrors: {
      render:
        'Er is helaas iets fout gegaan bij het weergeven van dit onderdeel.',
    },
  },
};
