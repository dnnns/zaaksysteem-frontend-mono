import { push } from 'connected-react-router';
import { Middleware } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { SAVE_COMMUNICATION } from './add/communication.add.constants';
import { fetchThreadsAction } from './threads/communication.threads.actions';
import { State } from '../../../store/interface';
import { CommunicationRootStateType } from './communication.reducer';
import { APICreateContactMomentRequestBody } from '../../../_generated/Communication.types';

const handleSaveCommunicationSuccess: MiddlewareHelper<
  State & CommunicationRootStateType,
  AjaxAction<{}, APICreateContactMomentRequestBody>
> = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    communication: {
      context: { rootPath },
    },
  } = state;
  const { dispatch } = store;
  const { thread_uuid } = action.payload;

  dispatch(fetchThreadsAction());
  dispatch(push(`${rootPath}/view/${thread_uuid}`));
};

export const communicationMiddleware: Middleware<
  {},
  State & CommunicationRootStateType
> = store => next => action => {
  switch (action.type) {
    case SAVE_COMMUNICATION.SUCCESS:
      return handleSaveCommunicationSuccess(store, next, action);
    default:
      return next(action);
  }
};

export default communicationMiddleware;
