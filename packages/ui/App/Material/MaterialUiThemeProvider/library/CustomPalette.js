import { createElement } from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { overrides, palette } from './theme';

const {
  MuiButton: { root, label },
} = overrides;

const {
  common: { white },
  error,
  review,
} = palette;

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root,
      label,
      containedPrimary: {
        color: white,
      },
    },
  },
  palette: {
    primary: review,
    secondary: error,
  },
  typography: {
    useNextVariants: true,
  },
});

export const CustomPalette = ({ children }) =>
  createElement(ThemeProvider, {
    theme,
    children,
  });
