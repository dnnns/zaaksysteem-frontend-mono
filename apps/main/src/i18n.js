import { initI18n } from '@zaaksysteem/common/src/i18n';
import { initReactI18next } from 'react-i18next';
import fecha from 'fecha';
import { addResourceBundle } from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './locale/common.locale';

function initFetcha(i18n) {
  fecha.i18n = {
    ...fecha.i18n,
    ...i18n.t('common:dates', { returnObjects: true }),
  };
}

export async function setupI18n() {
  const i18n = await initI18n(
    {
      lng: 'nl',
      keySeparator: '.',
      defaultNS: 'common',
      fallbackNS: 'common',
      interpolation: {
        escapeValue: false,
      },
    },
    [initReactI18next]
  );

  addResourceBundle(i18n, 'common', locale);
  initFetcha(i18n);
}
