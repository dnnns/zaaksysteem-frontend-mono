const normalizeItem = item =>
  item
    .split(' ')
    .join('-')
    .toLowerCase();

const createScopeValue = (scope, ...rest) =>
  [scope, ...rest]
    .filter(Boolean)
    .map(normalizeItem)
    .join(':');

const createScopeObject = (key, scope, ...rest) => {
  const value = createScopeValue(scope, ...rest);

  return scope ? { [key]: value } : {};
};

export const addScopeProp = (scope, ...rest) =>
  createScopeObject('scope', scope, ...rest);

export const addScopeAttribute = (scope, ...rest) =>
  createScopeObject('data-scope', scope, ...rest);
