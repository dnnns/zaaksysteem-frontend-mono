import React, { useState } from 'react';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { searchContact } from '../../../library/requests';
import { TYPE_COMPANY } from '../../../library/communicationEntityTypes.constants';

type Contact = {
  value: string;
  label: string;
  address: string;
  type: string;
  icon: React.ElementType;
};

const fetchContact = (input: string) =>
  searchContact(input)
    .then(response =>
      response.data.map(contact => ({
        value: contact.id,
        label: contact.name,
        subLabel: contact.address,
        type: contact.type,
        icon: (
          <ZsIcon size="small">
            {contact.type === TYPE_COMPANY
              ? 'entityType.company'
              : 'entityType.person'}
          </ZsIcon>
        ),
      }))
    )
    .catch(() => {});

interface ContactFinderProps {
  classes: { [key: string]: any };
  choices: {
    label: string;
    value: any;
    [key: string]: any;
  }[];
}

const ContactFinder: React.FunctionComponent<ContactFinderProps> = ({
  ...restProps
}) => {
  const [input, setInput] = useState('');
  const { choices } = restProps;

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={fetchContact}
    >
      {({ data, busy }: { data: Contact[] | null; busy: boolean }) => {
        const normalizedChoices = data || choices;

        return (
          <FlatValueSelect
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            getChoices={setInput}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default ContactFinder;
