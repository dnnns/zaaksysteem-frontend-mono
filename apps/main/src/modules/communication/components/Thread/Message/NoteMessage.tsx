import React from 'react';
import { useTranslation } from 'react-i18next';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import { NoteMessageType } from '../../../types/Message.types';

type NoteMessagePropsType = {
  message: NoteMessageType;
  caseNumber?: string;
};

const NoteMessage: React.FunctionComponent<NoteMessagePropsType> = ({
  message: {
    content,
    createdDate,
    sender: { name },
  },
  caseNumber,
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.note.messageTitle', {
    user: name,
  });
  const icon = 'threadType.note';

  return (
    <React.Fragment>
      <MessageHeader
        date={createdDate}
        title={title}
        icon={icon}
        caseNumber={caseNumber}
      />
      <MessageContent content={content} />
    </React.Fragment>
  );
};

export default NoteMessage;
