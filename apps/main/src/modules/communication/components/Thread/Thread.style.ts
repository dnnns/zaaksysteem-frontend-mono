import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const threadStyleSheet: StyleRulesCallback<any, {}> = () => ({
  threadWrapper: {
    width: '100%',
  },
  threadTitle: {
    marginLeft: '80px',
    marginBottom: '30px',
  },
});
