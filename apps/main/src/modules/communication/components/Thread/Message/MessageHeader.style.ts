import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const messageHeaderStyleSheet: StyleRulesCallback<any, {}> = ({
  typography,
  mintlab: { greyscale },
  palette: { primary },
}: any) => ({
  header: {
    display: 'flex',
    height: 48,
  },
  icon: {
    minWidth: 80,
    display: 'flex',
    justifyContent: 'center',
  },
  meta: {
    flexGrow: 1,
    fontWeight: typography.fontWeightMedium,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  title: {},
  right: {
    display: 'flex',
    alignItems: 'flex-end',
    flexDirection: 'column',
  },
  info: {
    color: greyscale.darker,
  },
  link: {
    color: primary.main,

    '&:hover, &:focus': {
      color: primary.darker,
    },
  },
});
