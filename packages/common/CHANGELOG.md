# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.1...@zaaksysteem/common@0.10.0) (2019-09-09)


### Features

* **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))





## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.0...@zaaksysteem/common@0.9.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/common





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.8.0...@zaaksysteem/common@0.9.0) (2019-09-05)


### Features

* MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.2...@zaaksysteem/common@0.8.0) (2019-08-29)


### Features

* **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
* **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
* **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))





## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.1...@zaaksysteem/common@0.7.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/common





## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.0...@zaaksysteem/common@0.7.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/common





# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.1...@zaaksysteem/common@0.7.0) (2019-08-06)


### Features

* **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))





## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.0...@zaaksysteem/common@0.6.1) (2019-07-31)

**Note:** Version bump only for package @zaaksysteem/common





# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.5.0...@zaaksysteem/common@0.6.0) (2019-07-31)


### Features

* **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))





# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.4.0...@zaaksysteem/common@0.5.0) (2019-07-30)


### Features

* **FormRenderer:** MINTY-1126 Move `FormRenderer` component with validation and translations from apps project to `@zaaksysteem/common` ([b9e0245](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b9e0245))





# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.3.0...@zaaksysteem/common@0.4.0) (2019-07-29)


### Features

* **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))





# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.2.0...@zaaksysteem/common@0.3.0) (2019-07-25)


### Features

* **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))





# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.1.0...@zaaksysteem/common@0.2.0) (2019-07-25)


### Features

* **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))





# 0.1.0 (2019-07-18)


### Features

* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
* **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
* **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
* **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
