/* eslint-disable no-unused-vars, no-console */
const fetch = require('node-fetch');
const { promisify } = require('util');
const fs = require('fs');
const { compile } = require('json-schema-to-typescript');
const $RefParser = require('json-schema-ref-parser');
const rimraf = require('rimraf');

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const mkDirAsync = promisify(fs.mkdir);
const rimrafAsync = promisify(rimraf);

const OUTPUT_PATH = './src/_generated/';
const DOMAIN = process.env.API_DOMAIN || 'https://development.zaaksysteem.nl';

async function fetchAndResolveSchema(baseUrl, fileName) {
  const response = await fetch(`${DOMAIN}${baseUrl}${fileName}`);
  const json = await response.json();
  const schema = await $RefParser.dereference(json, {
    resolve: {
      file: {
        async read(file, callback) {
          const [filename] = file.url.split('/').reverse();
          const fileResponse = await fetch(`${DOMAIN}${baseUrl}${filename}`);
          const fileJson = await fileResponse.json();

          callback(null, fileJson);
        },
      },
    },
  });

  return schema;
}

const sanitizeName = name => `API${name}`.replace('Entity', '');

function createOperationTypeName(operationId, postFix) {
  const typeName = `${operationId}`
    .split('_')
    .map(
      ([firstLetter, ...remaining]) =>
        `${firstLetter.toUpperCase()}${remaining.join('')}`
    )
    .join('');

  return sanitizeName(`${typeName}${postFix}`);
}

async function compileSchema(schema, name) {
  const type = await compile(schema, name, {
    declareExternallyReferenced: true,
    unreachableDefinitions: false,
    bannerComment: '/* eslint-disable */',
  });

  return type;
}

async function compileResponseBody(schema, operationId) {
  const { allOf, ...restSchema } = schema;
  return await compileSchema(
    restSchema,
    createOperationTypeName(operationId, 'ResponseBody')
  );
}

async function compileRequestBody(requestBody, operationId) {
  const { schema } =
    requestBody.content['multipart/form-data'] ||
    requestBody.content['application/json'];
  const { allOf, ...restSchema } = schema;

  const toCompile = Object.keys(restSchema).length ? restSchema : schema;
  return await compileSchema(
    toCompile,
    createOperationTypeName(operationId, 'RequestBody')
  );
}

async function compileRequestResponseTypes(schema) {
  const promises = Object.values(schema.paths).reduce((acc, pathValue) => {
    const allOperations = Object.values(pathValue).reduce(
      (operations, operationValue) => {
        const responseBody = compileResponseBody(
          operationValue.responses[200].content['application/json'].schema,
          operationValue.operationId
        );
        const requestBody = operationValue.requestBody
          ? compileRequestBody(
              operationValue.requestBody,
              operationValue.operationId
            )
          : null;

        return [...operations, responseBody, requestBody];
      },
      []
    );

    return [...acc, ...allOperations];
  }, []);

  const types = await Promise.all(promises);

  return types;
}

async function compileEntityTypes(schema) {
  const promises = Object.entries(schema.DDDEntities).map(([key, value]) => {
    return compileSchema(value, sanitizeName(key));
  });

  const types = await Promise.all(promises);

  return types;
}

async function writeTypes(output, outputPath) {
  return writeFileAsync(outputPath, output);
}

async function readSchemaFile() {
  const schema = await readFileAsync('schema.json', 'UTF-8');
  return JSON.parse(schema);
}

async function cleanOutputDir() {
  await rimrafAsync(OUTPUT_PATH);
  await mkDirAsync(OUTPUT_PATH);
}

async function start() {
  const startTime = Date.now();
  const schemas = await readSchemaFile();

  console.log(
    `Generating types for the following domains: ${Object.keys(
      schemas.domains
    ).join(', ')}`
  );
  console.warn(
    '⚠️  Marking all optional props as required remove this when back-end fixes their API spec'
  );

  const resolvedSchemas = await Promise.all(
    Object.entries(schemas.domains).map(
      async ([domain, { paths, entities, baseUrl }]) => {
        const pathsSchema = await fetchAndResolveSchema(baseUrl, paths);
        const entitiesSchema = await fetchAndResolveSchema(baseUrl, entities);

        return [domain, pathsSchema, entitiesSchema];
      }
    )
  );
  const types = await Promise.all(
    resolvedSchemas.map(async ([domain, paths, entities]) => {
      const pathsTypes = await compileRequestResponseTypes(paths);
      const entitiesTypes = await compileEntityTypes(entities);

      return [domain, pathsTypes, entitiesTypes];
    })
  );

  await cleanOutputDir();
  types.forEach(([domain, paths, entities]) => {
    const output = [...paths, ...entities].map(
      // temporarily mark all optional props as required until back-end fixes their API spec
      ts => (ts ? ts.replace(/\?/g, '') : ts)
    );
    const path = `${OUTPUT_PATH}${domain}.types.ts`;
    writeTypes(output.join('\n'), path);

    console.log(` - generated ${output.length} types in ${path}`);
  });

  const numTypes = types.reduce(
    (acc, [, paths, entities]) => acc + paths.length + entities.length,
    0
  );

  console.log(
    `⭐ Successfully generated ${numTypes} types in ${(Date.now() - startTime) /
      1000} seconds\n`
  );
}

start();
