import * as yup from 'yup';
import { STATUS_FAILED } from '../../Form/Constants/fileStatus';

/**
 * Creates an error when one or more files are
 * in the failed state.
 *
 * @param {Function} t
 * @param {Object} field
 * @return {Function}
 */
const allFilesValid = ({ t, field }) => {
  function validateFunc(value) {
    const failed = value.filter(
      thisValue => thisValue.status === STATUS_FAILED
    );

    if (failed.length) {
      return this.createError({
        message: t('validations:array.invalidFile'),
        path: field.name,
      });
    }
  }
  return yup
    .array()
    .required()
    .test('all-files-valid', null, validateFunc);
};

export default allFilesValid;
