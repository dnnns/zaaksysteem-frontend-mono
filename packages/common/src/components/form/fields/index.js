import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import ChoiceChips from '@mintlab/ui/App/Zaaksysteem/ChoiceChips';
import {
  TEXT,
  SELECT,
  CHECKBOX,
  FILE_SELECT,
  FLATVALUE_SELECT,
  EMAIL,
  TEXTAREA,
  CHOICE_CHIP,
} from '../constants/fieldTypes';
import FlatValueSelect from './FlatValueSelect';
import Textarea from './Textarea';
import FileSelect from './FileSelect';

export const FormFields = {
  [TEXT]: TextField,
  [SELECT]: Select,
  [FILE_SELECT]: FileSelect,
  [FLATVALUE_SELECT]: FlatValueSelect,
  [CHECKBOX]: Checkbox,
  [EMAIL]: TextField,
  [SELECT]: Select,
  [TEXT]: TextField,
  [TEXTAREA]: Textarea,
  [CHOICE_CHIP]: ChoiceChips,
};
