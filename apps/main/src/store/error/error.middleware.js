import { get } from '@mintlab/kitchen-sink/source';
import { login } from '@zaaksysteem/common/src/library/auth';
import { getUrl } from '@zaaksysteem/common/src/library/url';
import { showDialog } from '../ui/ui.actions';
import { DIALOG_ERROR } from '../../constants/dialog.constants';

/**
 * Handles request errors in a generic way.
 *
 * If the HTTP statuscode is 401, we redirect.
 *
 * If the response from the backend contains an error
 * array, we dispatch a Dialog action with an array of
 * translation keys, so we can display the most specific
 * error message possible.
 *
 * Else, we dispatch the dialog with a more generalized
 * translation key, based on the HTTP statuscode.
 *
 * If any of these keys cannot be found in the locale
 * file, the fallback key will be used.
 */

/**
 * @param {*} store
 * @return {Object}
 */
export const errorMiddleware = store => next => action => {
  const { dispatch } = store;

  const ajax = get(action, 'ajax');
  const error = get(action, 'error');

  /**
   * @param {string} errorCode
   * @return {Object}
   */
  const dispatchDialog = errorCode =>
    dispatch(
      showDialog({
        errorCode,
        dialogType: DIALOG_ERROR,
      })
    );

  if (ajax && error) {
    const { statusCode } = action;
    const { response } = action.payload;
    const { errors } = response;

    if (statusCode === 401) {
      dispatch(login(getUrl()));
      return next(action);
    }

    if (errors) {
      const [{ errorCode }] = errors;
      dispatchDialog(errorCode);

      return next(action);
    }

    dispatchDialog(statusCode);
  }

  return next(action);
};

export default errorMiddleware;
