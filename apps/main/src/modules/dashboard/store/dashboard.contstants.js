import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const FETCH_OPEN_CASES = createAjaxConstants('FETCH_OPEN_CASES');
