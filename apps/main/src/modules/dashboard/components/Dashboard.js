import React from 'react';
import { Link } from 'react-router-dom';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';

const Dashboard = ({ cases, loading }) => {
  if (loading) {
    return <Loader />;
  }

  return (
    <ul>
      {cases.map(item => (
        <li key={item.id}>
          <Link to={`/main/case/${item.id}/communication`}>
            {item.caseType} {item.caseNumber}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Dashboard;
