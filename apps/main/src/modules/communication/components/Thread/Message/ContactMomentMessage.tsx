import React from 'react';
import { useTranslation } from 'react-i18next';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import { ContactMomentMessageType } from '../../../types/Message.types';

type NameObjectType = { name: string };

interface ContactType {
  direction: string;
  sender: NameObjectType;
  recipient: NameObjectType;
}

type GetName = (options: ContactType) => string;

const getSenderName: GetName = ({ direction, sender, recipient }) =>
  (direction === 'incoming' ? recipient : sender).name;

const getRecipientName: GetName = ({ direction, sender, recipient }) =>
  (direction === 'incoming' ? sender : recipient).name;

type ContactMomentMessagePropsType = {
  message: ContactMomentMessageType;
};

const ContactMomentMessage: React.FunctionComponent<
  ContactMomentMessagePropsType
> = ({
  message: { channel, content, direction, createdDate, sender, recipient },
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.messageTitle', {
    sender: getSenderName({ direction, sender, recipient }),
    receiver: getRecipientName({ direction, sender, recipient }),
  });
  const info = `${t(`channels.${channel}`)} - ${t(`direction.${direction}`)}`;
  const icon = `channel.${channel}`;

  return (
    <React.Fragment>
      <MessageHeader date={createdDate} title={title} info={info} icon={icon} />
      <MessageContent content={content} />
    </React.Fragment>
  );
};

export default ContactMomentMessage;
