import i18n from 'i18next';
import { asArray } from '@mintlab/kitchen-sink/source';

export async function initI18n(options, use) {
  if (use) {
    asArray(use).map(item => i18n.use(item));
  }

  await i18n.init(options);

  return i18n;
}
