import React from 'react';
import { withStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { threadPlaceholderStyleSheet } from './ThreadPlaceholder.style';

type ThreadPlaceholderPropsType = {
  classes: any;
};

const ThreadPlaceholder: React.FunctionComponent<
  ThreadPlaceholderPropsType
> = ({ classes }) => {
  const [t] = useTranslation('communication');

  return (
    <div className={classes.wrapper}>
      <div className={classes.placeholder}>{t('placeholder')}</div>
    </div>
  );
};

export default withStyles(threadPlaceholderStyleSheet)(ThreadPlaceholder);
