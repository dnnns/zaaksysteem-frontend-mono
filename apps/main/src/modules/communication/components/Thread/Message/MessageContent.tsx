import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { messageContentStyleSheet } from './MessageContent.style';

type MessageContentPropsType = {
  classes: any;
  content: string;
};

const MessageContent: React.FunctionComponent<MessageContentPropsType> = ({
  classes,
  content,
}) => {
  return <div className={classes.content}>{content}</div>;
};

export default withStyles(messageContentStyleSheet)(MessageContent);
