import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const tabsStyleSheet: StyleRulesCallback<any, {}> = ({
  typography,
  palette: { primary },
  mintlab: { greyscale },
}: any) => ({
  tabsRoot: {
    marginBottom: '36px',
    minHeight: '60px',
  },

  tabsFlexContainer: {
    '&>:nth-child(2)': {
      margin: '0px 20px',
    },
  },
  selected: {},
  tabRoot: {
    flexBasis: 'auto',
  },
  tabWrapper: {
    flexDirection: 'row',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '18px',
    padding: '3px 20px',
    backgroundColor: 'transparent',
    color: greyscale.darkest,
    '$selected &': {
      color: primary.main,
      backgroundColor: primary.light,
    },
    ...typography.subtitle1,
    textTransform: 'lowercase',
    flexBasis: 'auto',
    '&&& > span': {
      margin: 0,
      marginRight: '12px',
    },
  },
  tabIndicator: {
    display: 'none',
  },
});

export default tabsStyleSheet;
