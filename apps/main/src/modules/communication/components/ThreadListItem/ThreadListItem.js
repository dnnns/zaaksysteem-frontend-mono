import React from 'react';
import { withStyles } from '@material-ui/styles';
import classNames from 'classnames';
import Icon from '@mintlab/ui/App/Material/Icon';
import { Link } from 'react-router-dom';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';

import Tag from './Tag/Tag';
import { threadListItemStyleSheet } from './ThreadListItem.style';
import DirectionIcon from './DirectionIcon/DirectionIcon';

const ThreadListItem = ({
  id,
  basePath,
  key,
  summary,
  classes,
  style,
  selected,
  icon,
  title,
  subTitle,
  unread,
  tag,
  hasAttachment,
  direction,
  date,
  scope,
}) => {
  const [t] = useTranslation('communication');

  return (
    <Link
      to={`${basePath}/view/${id}`}
      key={key}
      style={style}
      className={classNames(classes.wrapper, {
        [classes.unread]: unread,
        [classes.selected]: selected,
      })}
      {...addScopeAttribute(scope, 'link', id)}
    >
      <div className={classes.type}>{icon}</div>
      <div className={classes.content}>
        <p
          className={classNames(classes.contentTitle, {
            [classes.contentTitleUnread]: unread,
          })}
        >
          {title}
        </p>
        {subTitle && <p className={classes.contentSubTitle}>{subTitle}</p>}
        <p className={classes.contentSummary}>{summary}</p>
      </div>
      <div className={classes.info}>
        <div className={classes.infoTop}>
          {hasAttachment && <Icon size="small">attachment</Icon>}
          {direction && <DirectionIcon direction={direction} />}
          {<span>{fecha.format(date, t('thread.dateFormat'))}</span>}
        </div>
        {tag && (
          <div className={classes.infoBottom}>
            <Tag>{tag}</Tag>
          </div>
        )}
      </div>
    </Link>
  );
};

export default withStyles(threadListItemStyleSheet)(ThreadListItem);
