import React from 'react';
import Icon from '@mintlab/ui/App/Material/Icon';
import { withStyles } from '@material-ui/styles';
import { directionIconStyleSheet } from './DirectionIcon.style';

const DirectionIcon = ({ classes, direction }) => {
  return (
    <Icon size={16} classes={{ root: `${classes.root} ${classes[direction]}` }}>
      forward
    </Icon>
  );
};

export default withStyles(directionIconStyleSheet)(DirectionIcon);
