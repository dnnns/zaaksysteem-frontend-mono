import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const directionIconStyleSheet: StyleRulesCallback<any, {}> = () => ({
  root: {
    marginRight: 6,
  },
  incoming: {
    transform: 'rotate(90deg)',
  },
  outgoing: {
    transform: 'rotate(-45deg)',
  },
});
