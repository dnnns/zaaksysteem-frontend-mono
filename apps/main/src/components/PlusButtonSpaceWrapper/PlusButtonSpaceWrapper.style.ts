import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const plusButtonSpaceWrapperStyleSheet: StyleRulesCallback<
  any,
  {}
> = () => ({
  root: {
    width: 'inherit',
    '&:after': {
      content: '""',
      height: 80,
      display: 'block',
    },
  },
});
export default plusButtonSpaceWrapperStyleSheet;
