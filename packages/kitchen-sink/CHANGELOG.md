# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.5.0...@mintlab/kitchen-sink@3.5.1) (2019-09-06)

**Note:** Version bump only for package @mintlab/kitchen-sink





# [3.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.4.0...@mintlab/kitchen-sink@3.5.0) (2019-08-29)


### Features

* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))





# [3.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.3.2...@mintlab/kitchen-sink@3.4.0) (2019-07-31)


### Features

* **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))





## [3.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/kitchen-sink@3.3.1...@mintlab/kitchen-sink@3.3.2) (2019-07-25)

**Note:** Version bump only for package @mintlab/kitchen-sink





## [3.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@mintlab/kitchen-sink@3.3.0...@mintlab/kitchen-sink@3.3.1) (2019-07-23)

**Note:** Version bump only for package @mintlab/kitchen-sink





# 3.3.0 (2019-07-18)


### Features

* **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
* **KitchenSink:** MINTY-1120 Import kitchen-sink package into monorepo ([ee5f0bb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/ee5f0bb))
* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
