const { dictionary } = require('./dictionary');

/**
 * Create a dictionary that is not enumerable.
 *
 * @param {Object} object
 * @return {Object}
 */
export const expose = object =>
  dictionary(object, {
    enumerable: false,
  });
