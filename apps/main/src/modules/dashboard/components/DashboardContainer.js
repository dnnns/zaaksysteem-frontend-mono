import { connect } from 'react-redux';
import Dashboard from './Dashboard';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

const mapStateToProps = state => {
  if (!state.dashboard) return;

  const {
    dashboard: { cases, state: dashboardState },
  } = state;

  return { cases, loading: dashboardState === AJAX_STATE_PENDING };
};

const CaseContainer = connect(mapStateToProps)(Dashboard);

export default CaseContainer;
