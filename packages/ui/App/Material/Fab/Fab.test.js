import React from 'react';
import toJson from 'enzyme-to-json';
import { Fab } from './Fab';
import { shallow } from 'enzyme';

/**
 * @test {Fab}
 */
describe('The `Fab` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Fab
        aria-label={'ariaLabel'}
        color="primary"
        action={() => {}}
        scope={'fab'}
      >
        add
      </Fab>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
