import React from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core';
import { messageHeaderStyleSheet } from './MessageHeader.style';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';

const formatDate = (t, zulu) => {
  const newDate = new Date(zulu);
  const date = fecha.format(newDate, t('dates.dateFormatText'));
  const time = fecha.format(newDate, t('dates.timeFormat'));

  return `${date} ${time}`;
};

const MessageHeader = ({ classes, date, title, meta, icon }) => {
  const [t] = useTranslation('common');

  return (
    <div className={classes.header}>
      <div className={classes.icon}>
        <ZsIcon size={48}>{icon}</ZsIcon>
      </div>
      <div className={classes.info}>
        <div className={classes.title}>{title}</div>
        <div className={classes.meta}>{meta}</div>
      </div>
      <div className={classes.date}>{formatDate(t, date)}</div>
    </div>
  );
};

export default withStyles(messageHeaderStyleSheet)(MessageHeader);
