import React from 'react';
import { useTranslation } from 'react-i18next';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon/ZsIcon';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem from './ThreadListItem';

const ContactMomentThreadListItem = ({
  createdByName,
  withName,
  channel,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.title', {
    channel: t(`channels.${channel}`),
  });
  const subTitle = t('thread.contactMoment.subTitle', {
    createdByName,
    withName,
  });

  return (
    <ThreadListItem
      {...rest}
      title={title}
      subTitle={subTitle}
      icon={<ZsIcon size="medium">{`channel.${channel}`}</ZsIcon>}
      {...addScopeProp('thread', 'contactMoment')}
    />
  );
};

export default ContactMomentThreadListItem;
