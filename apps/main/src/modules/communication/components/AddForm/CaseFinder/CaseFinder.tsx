import React, { useState } from 'react';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { searchCase } from '../../../library/requests';

type CaseChoice = {
  value: string;
  label: string;
  subLabel: string;
};

const fetchCase = (input: string): Promise<CaseChoice[] | void> =>
  searchCase(input)
    .then(response =>
      response.data.map(caseItem => ({
        value: caseItem.id,
        label: `${caseItem.attributes.display_id}: ${caseItem.attributes.case_type_name}`,
        subLabel: caseItem.attributes.description,
      }))
    )
    .catch(() => {});

const CaseFinder = ({ ...restProps }) => {
  const [input, setInput] = useState('');
  const { choices } = restProps;

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={fetchCase}
    >
      {({ data, busy }: { data: CaseChoice[] | null; busy: boolean }) => {
        const normalizedChoices = data || choices;

        return (
          <FlatValueSelect
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            getChoices={setInput}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default CaseFinder;
