import { Reducer } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { COMMUNICATION_SET_CONTEXT } from './communincation.context.constants';
import { CommunicationSetContextActionPayload } from './communication.context.actions';

export interface CommunicationContextState {
  type: 'case' | 'contact' | null;
  caseUuid: string | null;
  contactUuid: string | null;
  contactName: string | null;
  rootPath: string;
}

const initialState: CommunicationContextState = {
  type: null,
  caseUuid: null,
  contactUuid: null,
  contactName: null,
  rootPath: '/',
};

const context: Reducer<
  CommunicationContextState,
  ActionWithPayload<CommunicationSetContextActionPayload>
> = (state = initialState, action) => {
  if (action.type === COMMUNICATION_SET_CONTEXT) {
    return {
      ...state,
      ...action.payload,
    };
  }

  return state;
};

export default context;
