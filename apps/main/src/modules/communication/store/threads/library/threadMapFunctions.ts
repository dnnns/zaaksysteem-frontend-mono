import { APIMessageThread } from '../../../../../_generated/Communication.types';
import {
  ThreadType,
  ContactMomentThreadType,
} from '../../../types/Thread.types';

export const mapGenericThreadProperties = (
  thread: APIMessageThread
): ThreadType => {
  return {
    id: thread.id,
    type: thread.attributes.thread_type,
    summary: thread.attributes.last_message.slug,
    createdByName: thread.attributes.last_message.created_name,
    date: new Date(thread.attributes.last_message.created),
    unread: false,
    tag: '',
  };
};

export const mapNoteThread = (thread: APIMessageThread): ThreadType => ({
  ...mapGenericThreadProperties(thread),
});

export const mapContactMomentThread = (
  thread: APIMessageThread
): ContactMomentThreadType => ({
  ...mapGenericThreadProperties(thread),
  direction: thread.attributes.last_message.direction,
  channel: thread.attributes.last_message.channel,
  withName: thread.attributes.last_message.recipient_name,
});
