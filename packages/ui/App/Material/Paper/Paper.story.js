import { React, stories, text } from '../../story';
import Paper from '.';

stories(module, __dirname, {
  Default() {
    return <Paper scope="story">{text('Greeting', 'Hello, world!')}</Paper>;
  },
});
