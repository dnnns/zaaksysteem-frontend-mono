# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [10.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.14.0...@mintlab/ui@10.15.0) (2019-09-09)


### Bug Fixes

* **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))


### Features

* **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))





# [10.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.13.0...@mintlab/ui@10.14.0) (2019-09-06)


### Features

* **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))





# [10.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.12.0...@mintlab/ui@10.13.0) (2019-08-29)


### Bug Fixes

* **Communication:** Make icons with background white ([72610f1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/72610f1))


### Features

* **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
* **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
* **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
* **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
* **Communication:** MINTY-1399 - add (temporary) colors for entitytypes ([4a0d023](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a0d023))
* **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
* **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
* **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
* **UI:** Add IconRounded and ZsIcon ([c042b11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c042b11))
* **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))





# [10.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.11.0...@mintlab/ui@10.12.0) (2019-08-27)


### Features

* **UI:** MINTY-1472 - add stories for viewing / generating elements of the theme ([0c9db5f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0c9db5f))





# [10.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.10.0...@mintlab/ui@10.11.0) (2019-08-12)


### Features

* **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))





# [10.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.1...@mintlab/ui@10.10.0) (2019-08-06)


### Features

* **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))





## [10.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.0...@mintlab/ui@10.9.1) (2019-07-31)

**Note:** Version bump only for package @mintlab/ui





# [10.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.3...@mintlab/ui@10.9.0) (2019-07-31)


### Features

* **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))





## [10.8.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.2...@mintlab/ui@10.8.3) (2019-07-25)

**Note:** Version bump only for package @mintlab/ui





## 10.8.2 (2019-07-23)

**Note:** Version bump only for package @mintlab/ui
