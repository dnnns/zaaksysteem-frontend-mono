import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

export interface ActionWithPayload<P> extends Action<string> {
  payload: P;
}

export type ActionPayloadWithResponse<P = {}, R = {}> = P & {
  response: R;
};

export type ThunkActionWithPayload<
  State,
  Payload,
  ExtraArgument = {}
> = ThunkAction<void, State, ExtraArgument, ActionWithPayload<Payload>>;
