import { combineReducers } from 'redux';
import items from './Items/items.reducer';
import caseTypeVersions from './CaseTypeVersions/caseTypeVersions.reducer';
import details from './Details/details.reducer';
import moveItems from './MoveItems/moveItems.reducer';
import changeOnlineStatus from './ChangeOnlineStatus/changeOnlineStatus.reducer';
import attribute from './Attribute/attribute.reducer';
import emailTemplate from './EmailTemplate/emailTemplate.reducer';
import folder from './Folder/folder.reducer';
import search from './Search/Search.reducer';
import documentTemplate from './DocumentTemplate/documentTemplate.reducer';

export const catalog = combineReducers({
  items,
  caseTypeVersions,
  details,
  moveItems,
  changeOnlineStatus,
  attribute,
  emailTemplate,
  folder,
  search,
  documentTemplate,
});

export default catalog;
