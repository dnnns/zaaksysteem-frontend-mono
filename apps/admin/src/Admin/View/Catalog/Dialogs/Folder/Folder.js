import React from 'react';
import { withStyles } from '@material-ui/styles';
import {
  Dialog,
  DialogTitle,
  DialogDivider,
  DialogContent,
  DialogActions,
} from '@mintlab/ui/App/Material/Dialog';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import { cloneWithout, unique } from '@mintlab/kitchen-sink/source';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import FormRenderer from '../../../Shared/Form/FormRenderer';
import { formStylesheet } from '../Shared/Form.style';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'contained'],
  secondaryPresets: ['text', 'contained'],
});

const renderField = ({ t, classes, values, doSave, isValid }) => ({
  FieldComponent,
  name,
  ...rest
}) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    classes,
    compact: true,
    name,
    key: `folder-form-component-${name}`,
    t,
    scope: `folder-form-component-${name}`,
    onKeyPress(event) {
      if (event.key.toLowerCase() === 'enter' && isValid) {
        doSave(values);
      }
    },
  };
  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};

/**
 * @return {ReactElement}
 */
const Folder = ({ t, classes, formDefinition, hide, saveAction, busy, id }) => {
  const doSave = values => saveAction({ values });
  const title = t('folder:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });
  const labelId = unique();
  const scope = 'catalog-folder-dialog';

  return (
    <Dialog
      id={scope}
      open={true}
      title={title}
      classes={classes}
      onClose={() => hide()}
      scope={scope}
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        icon="extension"
        id={labelId}
        title={title}
        classes={{
          rootElevated: classes.dialogTitle,
        }}
        onCloseClick={() => hide()}
        scope={scope}
      />

      <Render condition={busy}>
        <Loader />
      </Render>

      <Render condition={!busy}>
        <FormRenderer
          formDefinition={formDefinition}
          t={t}
          isInitialValid={id ? true : false}
        >
          {({ fields, values, isValid }) => {
            const formFields = fields.map(
              renderField({
                classes,
                t,
                values,
                doSave,
                isValid,
              })
            );
            const dialogActions = getDialogActions(
              {
                text: t('dialog:save'),
                disabled: busy || !isValid,
                action: () => doSave(values),
              },
              {
                text: t('dialog:cancel'),
                action: () => hide(),
              },
              'catalog-folder-dialog'
            );
            return (
              <React.Fragment>
                <DialogContent padded={true}>{formFields}</DialogContent>
                <React.Fragment>
                  <DialogDivider />
                  <DialogActions>{dialogActions}</DialogActions>
                </React.Fragment>
              </React.Fragment>
            );
          }}
        </FormRenderer>
      </Render>
    </Dialog>
  );
};

export default withStyles(formStylesheet)(Folder);
