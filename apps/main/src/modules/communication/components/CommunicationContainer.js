import { connect } from 'react-redux';
import Communication from './Communication';

const mapStateToProps = ({
  communication: {
    context: { rootPath, contactUuid: requestor },
  },
}) => ({
  requestor,
  rootPath,
});

const CommunicationContainer = connect(mapStateToProps)(Communication);

export default CommunicationContainer;
