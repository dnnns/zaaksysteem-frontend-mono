import { combineReducers } from 'redux';
import threads, {
  CommunicationThreadsState,
} from './threads/communication.threads.reducer';
import thread, {
  CommunicationThreadState,
} from './thread/communication.thread.reducer';
import add, { CommunicationAddState } from './add/communication.add.reducer';
import context, {
  CommunicationContextState,
} from './context/communication.context.reducer';

interface CommunicationState {
  context: CommunicationContextState;
  add: CommunicationAddState;
  thread: CommunicationThreadState;
  threads: CommunicationThreadsState;
}

export interface CommunicationRootStateType {
  communication: CommunicationState;
}

export const communication = combineReducers({
  threads,
  thread,
  add,
  context,
});

export default communication;
