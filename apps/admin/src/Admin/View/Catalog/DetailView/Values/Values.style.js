/**
 * @param {Object} theme
 * @return {JSS}
 */
export const valuesStyleSheet = ({ mintlab: { greyscale } }) => ({
  time: {
    marginLeft: '10px',
    color: greyscale.evenDarker,
  },
  default: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
});
