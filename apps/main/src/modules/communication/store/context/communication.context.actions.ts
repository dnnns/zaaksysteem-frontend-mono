import { COMMUNICATION_SET_CONTEXT } from './communincation.context.constants';
import { ActionCreator } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';

export type CommunicationSetContextActionPayload = {
  type: 'case' | 'contact';
  caseUuid?: string;
  contactUuid?: string;
  contactName?: string;
  rootPath: string;
};

export const setCommunicationContext: ActionCreator<
  ActionWithPayload<CommunicationSetContextActionPayload>
> = (context: CommunicationSetContextActionPayload) => ({
  type: COMMUNICATION_SET_CONTEXT,
  payload: context,
});
