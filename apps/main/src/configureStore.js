import { createBrowserHistory } from 'history';
import { createStore } from 'redux-dynamic-modules-core';
import { getThunkExtension } from 'redux-dynamic-modules-thunk';
import getRouterModule from './store/route/route.module';
import getSessionModule from './store/session/session.module';
import getUIModule from './store/ui/ui.module';
import getErrorModule from './store/error/error.module';

export const history = createBrowserHistory();

export const configureStore = initialState => {
  const store = createStore(
    { initialState, extensions: [getThunkExtension()] },
    getRouterModule(history),
    getSessionModule(),
    getUIModule(),
    getErrorModule()
  );

  return store;
};
