import React from 'react';
import { tagStyleSheet } from './Tag.style';
import { withStyles } from '@material-ui/styles';

const Tag = ({ children, classes }) => {
  return <span className={classes.wrapper}>{children}</span>;
};

export default withStyles(tagStyleSheet)(Tag);
