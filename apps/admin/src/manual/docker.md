# Docker

## `Dockerfile` heads up

### Permissions

Official node images come with a `node` user. That's because you 
shouldn't use `root`. Unfortunately the permissions fine print of 
`Dockerfile` instructions is slightly confusing and not well known.

- `USER` only affects `RUN`, `CMD` and `ENTRYPOINT`
- `WORKDIR` also **creates** directories that do not exist as `root`
- `COPY` ownership with `--chown` **is not recursive** by design

In other words:

- create and change ownership of deep structures with a `RUN` instruction early on
- use `COPY` with `--chown` for operations that do not include recursion
- do not invoke `WORKDIR` when it has side effects

### DRY by coincidence

- `ARG`
    - default values are assigned before the first `FROM`
    - default values can be used **in** `FROM`
    - after `FROM` an `ARG` must be included in that build stage
    - the **main** purpose of `ARG` is not to avoid path repetition
      in the `Dockerfile` but to serve as a single source of truth by 
      passing it with `ENV` to the build tools in the container

## VCS working copy dependencies

The VCS working copies of `zaaksysteem-apps` and `zaaksysteem` MUST 
be siblings in the local file system and their directory names MUST 
equal their respective repository names.

- /some/local/path
    - `/zaaksysteem`
        - `docker-compose(.override)?.yml`
            - `apps` service
    - `/zaaksysteem-apps`
        - `apps` Dockerfile

See [Development](./development.html) for usage.

## The *Node.js* nitty-gritty

The following describes details that are not necessary 
to know or understand for development purposes. As of 2019,
this strategy has been working faultless for several months.

A *Node* front-end project basically looks like

- `/local/project`
    - `<source-code>`
    - `/node_modules`
    - `package.json`
    - `package-lock.json`

where

1. `package(-lock)?.json` are under version control
2. `node_modules` is a sibling directory and **not** under version control
3. `package.json` declares
    1. the project dependencies that are installed under `node_modules`
    2. executable `scripts` that use binaries in `node_modules`
       to compile library packages in `node_modules` and 
       `<source-code>` into a JS bundle

In a *Docker* project, you would mount `/local/project` as a volume
in the container for development. Now you have two problems:

1. `node_modules` are installed on your host file system;
   among other concerns, malicious `postinstall` scripts 
   have a foot in the door now
2. the *Docker for Mac* 
   [`osxfs` is a dog slow mess](https://github.com/docker/for-mac/issues/1592) 

But let's remember that we all want `osxfs` for its file event support,
because before it *Docker* was de facto useless for front-end developers 
on *macOS*.

Let's also remember that there are no de facto front-end developers
using *GNU/Linux*.

### The mandatory quixotic *Sturm und Drang* period

The common workaround is to use a named volume for the offending 
`node_modules` directory; this sort of works, until it doesn't,
because it
 
1. holds the largest amount of files by a long shot
2. you don't want it on your host file system anyway 

On the flip side

1. it's yet another hack that you need to document, or explain on demand
2. named volumes do spontaneously unmount with *Docker for Mac*;
   (for me, that happened about 4 to 8 times an hour)

We've all been there, and we've all been looking for a new job 
after a while, without *Docker*.

### If you can't beat them, exercise a bit, and beat them harder

The solution that we use in this repository is actually quite simple: 
the structure of the container does not need to reflect the structure 
of the repository.

The core of the project under version control is:

- `/local/zaaksysteem-apps`
    - `apps` (source code)
    - `node` (stuff we only use in the container)
    - `npm` (`package(-lock).json` files)

In the container, we mount `/apps` in `/node` as `./src`
and symlink the `package(-lock).json` files to `/npm`.
After installation, `node_modules` are neither under 
version control nor available on the host and do not 
dangle in some 'shared' volume.

- `/opt-zaaksysteem-apps`
    - `/node`
        - `/node_modules`
        - `/src`: volume with the source code in `/apps`
        - `/vendor`
            - `/node_modules`
            - `package.json -> ../../npm/vendor/package.json`
            - `package-lock.json -> ../../npm/vendor/package.json`
        - `package.json -> ../npm/build/package.json`
        - `package-lock.json -> ../npm/build/package.json`
    - `/npm`
        - `/build`
            - `package.json`
            - `package-lock.json`
        - `/vendor`
            - `package.json`
            - `package-lock.json`
