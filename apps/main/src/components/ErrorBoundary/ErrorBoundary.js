import React from 'react';
import ReactErrorBoundary from 'react-error-boundary';
import { useTranslation } from 'react-i18next';
import { Body1, Body2 } from '@mintlab/ui/App/Material/Typography';
import Icon from '@mintlab/ui/App/Material/Icon';
import { errorBoundaryStylesheet } from './ErrorBoundary.style';
import { withStyles } from '@material-ui/styles';

const FallbackComponent = ({ classes, componentStack }) => {
  const [t] = useTranslation('common');
  return (
    <div className={classes.wrapper} title={componentStack}>
      <div className={classes.titleWrapper}>
        <span className={classes.icon}>
          <Icon size={48}>error_outline</Icon>
        </span>
        <Body1>{t('clientErrors.render')}</Body1>
      </div>

      {process.env.NODE_ENV === 'development' ? (
        <Body2 classes={{ root: classes.stacktrace }}>{componentStack}</Body2>
      ) : null}
    </div>
  );
};

const FallbackComponentWithStyles = withStyles(errorBoundaryStylesheet)(
  FallbackComponent
);

const ErrorBoundary = ({ children, ...props }) => (
  <ReactErrorBoundary
    FallbackComponent={FallbackComponentWithStyles}
    {...props}
  >
    {children}
  </ReactErrorBoundary>
);

export default ErrorBoundary;
