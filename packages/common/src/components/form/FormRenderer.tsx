import React, { Component, ComponentType } from 'react';
import { withFormik } from 'formik';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { withTranslation, WithTranslation } from 'react-i18next';
import { FormFields } from './fields';
import createValidation from './validation/createValidation';
import defaultValidationMap from './validation/validationMap';
import { withRules, Rule } from './rules';
import locale from './locale/locale';
import I18nResourceBundle from '../i18nResourceBundle/I18nResourceBundle';
import {
  FormDefinitionField,
  FormValue,
  FormDefinition,
} from './types/formDefinition.types';
import { Form } from './library/Form';
import i18next from 'i18next';
import { ValidationRule } from './validation/validationRules';

const getValuesFromDefinition = (
  formDefinition: FormDefinitionField[]
): { [key: string]: FormValue } =>
  formDefinition.reduce(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {}
  );

interface FormRendererProps extends WithTranslation {
  formDefinition: FormDefinitionField[];
  rules?: Rule[];
  isInitialValid?: boolean;
  FieldComponents?: {
    [key: string]: ComponentType<any>;
  };
  validationMap?: {
    [key: string]: ValidationRule;
  };
  t: i18next.TFunction;
}

interface FormRendererState {
  formDefinition: FormDefinitionField[];
}

// eslint-disable valid-jsdoc
class FormRenderer extends Component<FormRendererProps, FormRendererState> {
  private WrappedForm: ComponentType<any>;

  static defaultProps = {
    rules: [],
  };

  constructor(props: FormRendererProps) {
    super(props);
    this.state = {
      formDefinition: props.formDefinition,
    };

    const FormComponent =
      props.rules && props.rules.length > 0
        ? withRules({
            Component: Form,
            setFormDefinition: this.setFormDefinition,
          })
        : Form;

    this.WrappedForm = withFormik({
      enableReinitialize: false,
      isInitialValid:
        typeof this.props.isInitialValid === 'undefined'
          ? true
          : this.props.isInitialValid,
      validate: this.validate,
      mapPropsToValues: () =>
        getValuesFromDefinition(this.state.formDefinition),
      handleSubmit() {},
    })(FormComponent as ComponentType<any>);
  }

  render() {
    const { rules, children, FieldComponents = {}, ...rest } = this.props;

    const Fields = {
      ...FormFields,
      ...FieldComponents,
    };

    const WrappedForm = this.WrappedForm;

    return (
      <I18nResourceBundle resource={locale} namespace="validations">
        <WrappedForm
          Fields={Fields}
          formDefinition={this.state.formDefinition}
          rules={rules}
          {...cloneWithout(rest, 'formDefinition')}
        >
          {children}
        </WrappedForm>
      </I18nResourceBundle>
    );
  }

  setFormDefinition = (formDefinition: FormDefinition) =>
    this.setState({ formDefinition });

  validate = (values: { [key: string]: FormValue }) => {
    const { validationMap, t } = this.props;
    const { formDefinition } = this.state;

    const mergedValidationMap = {
      ...defaultValidationMap,
      ...validationMap,
    };

    const validation = createValidation({
      formDefinition,
      validationMap: mergedValidationMap,
      values,
      t,
    });

    return validation.then(yupErrors => {
      if (Object.keys(yupErrors).length === 0) return;

      const errors = yupErrors.inner.reduce<{ [key: string]: string }>(
        (acc, error) => {
          const { message, path } = error;
          if (message && !acc[path]) {
            acc[path] = message;
          }
          return acc;
        },
        {}
      );

      if (errors) {
        throw errors;
      }
    });
  };
}

export default withTranslation()(FormRenderer);
