import React, { Suspense } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { withStyles } from '@material-ui/styles';
import { compose } from 'recompose';
import { appStyleSheet } from './App.style';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';

const CaseModule = React.lazy(() =>
  import(/* webpackChunkName: "case" */ './modules/case')
);

const DashboardModule = React.lazy(() =>
  import(/* webpackChunkName: "dashboard" */ './modules/dashboard')
);

const ContactModule = React.lazy(() =>
  import(/* webpackChunkName: "contact" */ './modules/contact')
);

const Routes = ({ ready, classes, prefix, userId }) => {
  if (!ready) {
    return <Loader delay="200" />;
  }

  return (
    <Suspense fallback={<Loader delay="200" />}>
      <div className={classes.app}>
        <ErrorBoundary>
          <Switch>
            <Route path={`${prefix}/case/:caseId`} component={CaseModule} />
            <Route
              path={`${prefix}/contact/:contactId`}
              component={ContactModule}
            />
            <Route
              path="*"
              render={props => <DashboardModule {...props} userId={userId} />}
            />
          </Switch>
        </ErrorBoundary>
      </div>
    </Suspense>
  );
};

const mapStateToProps = ({ session: { state, data } }) => {
  if (state !== AJAX_STATE_VALID || !data.logged_in_user) {
    return {
      ready: false,
    };
  }

  return {
    ready: true,
    userId: data.logged_in_user.id,
  };
};

export default compose(
  connect(mapStateToProps),
  withStyles(appStyleSheet)
)(Routes);
