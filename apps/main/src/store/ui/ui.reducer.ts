import { SHOW_DIALOG, HIDE_DIALOG } from './ui.constants';
import { DialogAction, DialogActionPayload } from './ui.actions';

export interface UIState {
  dialogs: DialogActionPayload[];
}

const initialState: UIState = {
  dialogs: [],
};

export default function ui(
  state: UIState = initialState,
  action: DialogAction
): UIState {
  const { payload } = action;

  switch (action.type) {
    case SHOW_DIALOG:
      return {
        ...state,
        dialogs: [...state.dialogs, payload],
      };

    case HIDE_DIALOG:
      return {
        ...state,
        dialogs: state.dialogs.filter((dialog, index) => {
          if (payload && payload.dialogType) {
            return payload.dialogType !== dialog.dialogType;
          }

          return index !== state.dialogs.length - 1;
        }),
      };

    default:
      return state;
  }
}
