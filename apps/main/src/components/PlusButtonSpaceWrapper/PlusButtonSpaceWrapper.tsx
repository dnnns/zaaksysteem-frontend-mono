import React from 'react';
import { withStyles } from '@material-ui/styles';
import plusButtonSpaceWrapperStyleSheet from './PlusButtonSpaceWrapper.style';

interface PlusButtonSpaceWrapperProps {
  classes: { [key: string]: any };
}

const PlusButtonSpaceWrapper: React.FunctionComponent<
  PlusButtonSpaceWrapperProps
> = ({ children, classes }) => <div className={classes.root}>{children}</div>;

export default withStyles(plusButtonSpaceWrapperStyleSheet)(
  PlusButtonSpaceWrapper
);
