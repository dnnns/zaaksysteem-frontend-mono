import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { CASE_FETCH } from './case.constants';

const fetchCaseAction = createAjaxAction(CASE_FETCH);

export const fetchCase = caseId => {
  const url = `/api/v1/case/${caseId}`;

  return fetchCaseAction({
    method: 'GET',
    url,
    payload: {
      caseId,
    },
  });
};
