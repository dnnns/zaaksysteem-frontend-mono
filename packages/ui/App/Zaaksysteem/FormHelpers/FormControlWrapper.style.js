const marginBottom = '6px';
const paddingTop = '8px';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formControlWrapperStylesheet = ({
  mintlab: { greyscale, radius },
}) => ({
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
  },
  label: {
    flex: '1 0 auto',
    marginBottom,
  },
  help: {
    width: '50px',
    '& > div': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: '4px',
    },
  },
  error: {
    width: '100%',
  },
  hint: {
    width: '100%',
    color: greyscale.evenDarker,
    marginBottom,
  },
  control: {
    width: '100%',
    marginBottom,
    borderRadius: radius.defaultFormElement,
  },
  controlBackgroundColor: {
    backgroundColor: greyscale.light,
  },
  colLabels: {
    width: '250px',
    marginRight: '10px',
    paddingTop,
  },
  colContent: {
    flex: '1 0 auto',
  },
  colHelp: {
    paddingTop,
  },
});
