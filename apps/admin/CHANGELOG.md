# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.1...@zaaksysteem/admin@0.5.2) (2019-09-09)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.0...@zaaksysteem/admin@0.5.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.4.0...@zaaksysteem/admin@0.5.0) (2019-09-05)


### Features

* MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))





# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.2...@zaaksysteem/admin@0.4.0) (2019-08-29)


### Features

* **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
* **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
* **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
* **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))





## [0.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.1...@zaaksysteem/admin@0.3.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.0...@zaaksysteem/admin@0.3.1) (2019-08-22)


### Bug Fixes

* **admin:** add trailing slash to homepage ([eb5c176](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eb5c176))





# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.2.0...@zaaksysteem/admin@0.3.0) (2019-08-22)


### Features

* **admin:** Always start from homepage ([0375a5d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0375a5d))
* **Admin:** Activate new catalog for trial and master ([7e3a731](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7e3a731))
* **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))





# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.1...@zaaksysteem/admin@0.2.0) (2019-08-12)


### Features

* **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))





## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.0...@zaaksysteem/admin@0.1.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/admin





# 0.1.0 (2019-08-06)


### Features

* **Admin:** MINTY-1281 Add admin build to nginx ([2b58ef2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b58ef2))
* **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))
