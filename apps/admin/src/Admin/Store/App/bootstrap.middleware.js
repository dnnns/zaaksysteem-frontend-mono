import { APP_BOOTSTRAP_INIT, APP_BOOTSTRAP_VALID } from './app.constants';
import { fetchSession } from '../Session/session.actions';
import { SESSION_FETCH } from '../Session/session.constants';
import { valid, error } from './app.actions';
import { invoke } from '../Route/route.actions';
import { getUrl } from '../../../library/url';

const handleBootstrapStart = (store, next, action) => {
  next(action);
  store.dispatch(fetchSession());
};

const handleSessionFetchSuccess = (store, next, action) => {
  next(action);
  store.dispatch(valid());
};

const handleSessionFetchError = (store, next, action) => {
  next(action);
  store.dispatch(error());
};

const handleBootstrapValid = (store, next, action) => {
  next(action);
  store.dispatch(
    invoke({
      path: getUrl(),
    })
  );
};

export const bootstrapMiddleware = store => next => action => {
  switch (action.type) {
    case APP_BOOTSTRAP_INIT:
      return handleBootstrapStart(store, next, action);
    case APP_BOOTSTRAP_VALID:
      return handleBootstrapValid(store, next, action);
    case SESSION_FETCH.SUCCESS:
      return handleSessionFetchSuccess(store, next, action);
    case SESSION_FETCH.ERROR:
      return handleSessionFetchError(store, next, action);
    default:
      return next(action);
  }
};

export default bootstrapMiddleware;
