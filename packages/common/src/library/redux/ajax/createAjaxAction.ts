/* eslint-disable valid-jsdoc */
import { request } from '../../fetch';
import { AjaxActionConstants } from './createAjaxConstants';
import { Dispatch } from 'redux';
import {
  ActionWithPayload,
  ActionPayloadWithResponse,
} from 'packages/common/src/types/ActionWithPayload';

type CreateAjaxAction = (
  constants: AjaxActionConstants
) => (data: {
  method: 'GET' | 'POST' | 'PUT';
  url: string;
  data?: any;
  payload?: any;
  headers?: any;
}) => (dispath: Dispatch<AjaxAction>) => Promise<any>;

export interface AjaxAction<R = {}, P = {}>
  extends ActionWithPayload<ActionPayloadWithResponse<P, R>> {
  ajax: boolean;
  error?: boolean;
  statusCode?: number;
}

const parseResponseBody = (response: Response): Promise<any> => {
  let clone = response.clone();

  return response
    .json()
    .then(body => body)
    .catch(() => clone.text().then(body => body));
};

export const createAjaxAction: CreateAjaxAction = constants => ({
  method,
  url,
  data,
  payload,
  headers,
}) => dispatch => {
  dispatch({
    type: constants.PENDING,
    ajax: true,
    payload: {
      ...payload,
    },
  });

  const merge = (body: any) => ({
    ajax: true,
    payload: {
      ...payload,
      response: body,
    },
  });

  return request(method, url, data, headers)
    .then(response => {
      return parseResponseBody(response).then(body => {
        dispatch({
          ...merge(body),
          type: constants.SUCCESS,
        });
      });
    })
    .catch(response => {
      return parseResponseBody(response).then(body => {
        dispatch({
          ...merge(body),
          type: constants.ERROR,
          error: true,
          statusCode: response.status,
        });
      });
    });
};
