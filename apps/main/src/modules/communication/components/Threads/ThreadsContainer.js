import { connect } from 'react-redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

import Threads from './Threads';
import { fetchThreadsAction } from '../../store/threads/communication.threads.actions';

const mapStateToProps = ({
  communication: {
    uuid,
    threads: { threads, state },
    context: { rootPath },
  },
}) => {
  return {
    uuid,
    threads,
    rootPath,
    busy: state === AJAX_STATE_PENDING,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchThreads: () => dispatch(fetchThreadsAction()),
});

const ThreadsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Threads);

export default ThreadsContainer;
