import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import ThreadTitle from './ThreadTitle';
import { NoteMessageType } from '../../../types/Message.types';

type NoteThreadTitlePropsType = {
  classes: any;
  message: NoteMessageType;
};

const NoteThreadTitle: React.FunctionComponent<NoteThreadTitlePropsType> = ({
  classes,
  message,
}) => {
  const [t] = useTranslation('communication');
  const createdByName = get(message, 'sender.name');
  const title = t('thread.note.title', { createdByName });

  return <ThreadTitle classes={classes} title={title} />;
};

export default NoteThreadTitle;
