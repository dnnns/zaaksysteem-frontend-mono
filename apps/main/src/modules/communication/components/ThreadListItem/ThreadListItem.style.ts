import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const threadListItemStyleSheet: StyleRulesCallback<any, {}> = ({
  typography,
  palette: { primary },
  mintlab: { greyscale },
}) => ({
  wrapper: {
    display: 'flex',
    borderBottom: `1px solid ${greyscale.dark}`,
    padding: '20px 24px 20px 14px',
    textDecoration: 'none',
    height: '100%',
    position: 'relative',
  },
  unread: {
    '&:before': {
      content: '""',
      width: '4px',
      height: '100%',
      backgroundColor: primary.main,
      position: 'absolute',
      left: 0,
      top: 0,
    },
  },
  selected: {
    backgroundColor: primary.lighter,
  },

  /** Icon column */
  type: {
    width: '50px',
    display: 'flex',
    justifyContent: 'center',
    color: 'white',
    paddingRight: '16px',
  },
  /** Content column */
  content: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
  },
  contentTitle: {
    ...typography.subtitle2,
    color: greyscale.darkest,
    padding: 0,
    margin: 0,
    marginBottom: '4px',
    fontWeight: typography.fontWeightBold,
  },
  contentTitleUnread: {
    fontWeight: 'bold',
  },
  contentSubTitle: {
    ...typography.subtitle2,
    fontWeight: 400,
    padding: 0,
    margin: 0,
    color: greyscale.evenDarker,
    marginBottom: '6px',
  },
  contentSummary: {
    ...typography.body2,
    padding: 0,
    margin: 0,
    color: greyscale.darkest,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    lineHeight: 'normal',
  },

  /** Info column */
  info: {
    width: '100px',
    display: 'flex',
    flexDirection: 'column',
  },
  infoTop: {
    ...typography.caption,
    display: 'flex',
    color: greyscale.evenDarker,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  infoBottom: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
