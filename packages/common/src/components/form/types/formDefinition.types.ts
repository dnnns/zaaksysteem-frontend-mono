import { FormikProps, FormikActions, FormikErrors } from 'formik';
import { ComponentType } from 'react';

export type FlatFormValue = string | number;
export type NestedFormValue = { value: FlatFormValue };
export type FormValue = FlatFormValue | NestedFormValue | null;

export interface FormDefinitionField {
  type: string;
  name: string;
  hidden?: boolean;
  required: boolean;
  disabled?: boolean;
  value?: FormValue;
  placeholder?: string;
  label?: string;
  refValue?: string | FormValue | null;
  choices?: any[];
  [key: string]: any;
}

export type FormDefinition = FormDefinitionField[];

export interface WrappedFormProps {
  formDefinition: FormDefinitionField[];
}

export interface FormRendererFormField
  extends FormDefinitionField,
    Pick<FormikActions<any>, 'setFieldValue'> {
  definition: FormDefinitionField;
  FieldComponent: ComponentType;
  value: FormValue;
  checked: FormValue;
  error?: FormikErrors<any> | string;
  touched: boolean;
  key: string;
  refValue: FormValue | null;
  onChange: (event: React.SyntheticEvent<HTMLInputElement>) => void;
  onBlur: (event: React.SyntheticEvent) => void;
}

export interface FormRendererRenderProps
  extends FormikProps<any>,
    Pick<FormikActions<any>, 'setFieldValue' | 'setFieldTouched'> {
  fields: FormRendererFormField[];
}
