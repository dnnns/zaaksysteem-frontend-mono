/**
 * @return {JSS}
 */
export const layoutStylesheet = () => ({
  next: {
    padding: '0 20px',
  },
  legacy: {
    padding: '20px',
  },
});
