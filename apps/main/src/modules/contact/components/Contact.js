import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '../../communication';

const Contact = () => {
  return (
    <Switch>
      <Route
        path={`/:prefix/contact/:contactId/communication`}
        render={({ match }) => (
          <CommunicationModule
            type="contact"
            contactUuid={match.params.contactId}
            rootPath={match.url}
          />
        )}
      />
    </Switch>
  );
};

export default Contact;
