import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getCaseModule } from './store/case.module';
import CaseContainer from './components/CaseContainer';
import locale from './locale/case.locale';

export default function CaseModule({ match }) {
  return (
    <DynamicModuleLoader modules={[getCaseModule(match.params.caseId)]}>
      <I18nResourceBundle resource={locale} namespace="case">
        <CaseContainer />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
}
