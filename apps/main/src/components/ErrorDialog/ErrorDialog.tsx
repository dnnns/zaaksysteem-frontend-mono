import React from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
import useMessages from '../../library/useMessages';

interface ErrorDialogProps {
  errorCode: string;
  onClose?: () => void;
  [key: string]: any;
}

const ErrorDialog: React.FunctionComponent<ErrorDialogProps> = ({
  errorCode,
  onClose,
  ...restProps
}) => {
  const [t] = useTranslation('common');
  const [messages] = useMessages();

  const registeredMessage = messages[errorCode];
  const defaultProps = {
    ...restProps,
    key: errorCode,
    primaryButton: { text: t('dialog.ok'), action: onClose },
    title: t('dialog.error.title'),
    body: t('dialog.error.body'),
  };

  const props = registeredMessage
    ? {
        ...defaultProps,
        body: registeredMessage,
      }
    : defaultProps;

  const { body, ...alertRestProps } = props;

  return (
    // @ts-ignore
    <Alert {...alertRestProps} onClose={onClose} open={true}>
      {body}
    </Alert>
  );
};

export default ErrorDialog;
