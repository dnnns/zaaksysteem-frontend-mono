import React from 'react';
import { useTranslation } from 'react-i18next';
import ThreadTitle from './ThreadTitle';
import { ContactMomentMessageType } from '../../../types/Message.types';

type ContactMomentThreadTitlePropsType = {
  classes: any;
  message: ContactMomentMessageType;
};

const ContactMomentThreadTitle: React.FunctionComponent<
  ContactMomentThreadTitlePropsType
> = ({ classes, message: { channel } }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.title', {
    channel: t(`channels.${channel}`),
  });

  return <ThreadTitle classes={classes} title={title} />;
};

export default ContactMomentThreadTitle;
