const { WEBPACK_BUILD_TARGET } = process.env;

const defaultPlugins = [
  '@babel/plugin-proposal-class-properties',
  '@babel/plugin-syntax-dynamic-import',
];

const productionPlugins = [
  [
    'react-remove-properties',
    {
      properties: ['scope', 'data-scope'],
    },
  ],
];

const config = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    ...defaultPlugins,
    ...(WEBPACK_BUILD_TARGET === 'production' ? productionPlugins : []),
  ],
  env: {
    test: {
      presets: [
        [
          '@babel/preset-env',
          {
            targets: { node: 'current' },
          },
        ],
      ],
      plugins: ['babel-plugin-dynamic-import-node'],
    },
  },
};

module.exports = function(api) {
  api.cache(true);
  return config;
};
