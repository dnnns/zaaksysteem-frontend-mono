import React, { useEffect } from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getCommunicationModule } from './store/communication.module';
import CommunicationContainer from './components/CommunicationContainer';
import locale from './locale/communication.locale';
import useMessages from '../../library/useMessages';

export default function CommunicationModule({
  contactUuid,
  contactName,
  caseUuid,
  rootPath,
  type,
}) {
  const [t] = useTranslation('communication');
  const [, addMessages, removeMessages] = useMessages();

  useEffect(() => {
    const messages = t('serverErrors', {
      returnObjects: true,
    });

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  return (
    <DynamicModuleLoader
      modules={[
        getCommunicationModule({
          contactUuid,
          contactName,
          caseUuid,
          type,
          rootPath,
        }),
      ]}
    >
      <I18nResourceBundle resource={locale} namespace="communication">
        <CommunicationContainer />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
}
