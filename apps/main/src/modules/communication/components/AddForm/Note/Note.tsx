import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
import { applyTranslations } from '@zaaksysteem/common/src/library/applyTranslations';
import {
  FormDefinition,
  FormRendererRenderProps,
  FormRendererFormField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { Button } from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Render from '@mintlab/ui/App/Abstract/Render';
import { TYPE_NOTE } from '../../../library/communicationTypes.constants';
import { addFormStyleSheet } from './../AddForm.style';

const renderField = ({ classes }: { classes: { [key: string]: string } }) => {
  return function RenderFieldComponent({
    FieldComponent,
    name,
    error,
    touched,
    ...rest
  }: FormRendererFormField) {
    const props = {
      ...cloneWithout(rest, 'type', 'mode', 'classes'),
      compact: true,
      name,
      key: `note-component-${name}`,
      scope: `note-component-${name}`,
    };

    return (
      <div className={classNames(classes.formRow)} key={props.key}>
        <FieldComponent {...props} />
        <Render condition={touched && error}>
          <div className={classes.error}>{error}</div>
        </Render>
      </div>
    );
  };
};

export interface NotePropsType {
  save: (options: { [key: string]: any }) => void;
  busy: boolean;
  classes: any;
  formDefinition: FormDefinition;
  rootPath: string;
}

const Note: React.FunctionComponent<NotePropsType> = ({
  save,
  busy,
  classes,
  formDefinition,
  rootPath,
}) => {
  const [t] = useTranslation();
  const translatedFormDefinition = applyTranslations(formDefinition, t);
  const renderForm = ({ fields, values, isValid }: FormRendererRenderProps) => {
    const formFields = fields.map(renderField({ classes }));
    const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
      <Link to={rootPath} innerRef={ref} {...props} />
    ));
    LinkComponent.displayName = 'cancelLinkComponent';

    return (
      <div className={classes.formWrapper}>
        {formFields}

        <div className={classes.buttons}>
          <Button component={LinkComponent} presets={['text', 'contained']}>
            {t('common:forms.cancel')}
          </Button>
          <Button
            disabled={!isValid || busy}
            action={() => {
              save({
                type: TYPE_NOTE,
                values,
              });
            }}
            presets={['primary', 'contained']}
          >
            {t('common:forms.add')}
          </Button>
        </div>
      </div>
    );
  };

  return (
    <FormRenderer
      formDefinition={translatedFormDefinition}
      isInitialValid={false}
    >
      {renderForm}
    </FormRenderer>
  );
};

export default withStyles(addFormStyleSheet)(Note);
