import React from 'react';
import { withStyles } from '@material-ui/styles';
import { components } from 'react-select';
import { Body1, Body2 } from '../../../Material/Typography';
import { multilineOptionStyleSheet } from './MultilineOption.style';

const MultilineOption = ({ data, label, classes, ...rest }) => {
  const { icon, subLabel } = data;

  return (
    <components.Option {...rest}>
      <div className={classes.optionWrapper}>
        {icon && <div className={classes.icon}>{icon}</div>}
        <div className={classes.labelWrapper}>
          <div className={classes.optionLabel}>
            <Body1>{label}</Body1>
          </div>
          {subLabel && (
            <div className={classes.subLabel}>
              <Body2>{subLabel}</Body2>
            </div>
          )}
        </div>
      </div>
    </components.Option>
  );
};

/* eslint-disable react/display-name*/
const ForwardRef = React.forwardRef(function ForwardRefMultilineOption(
  props,
  ref
) {
  return <MultilineOption innerRef={ref} {...props} />;
});

export default withStyles(multilineOptionStyleSheet)(ForwardRef);
