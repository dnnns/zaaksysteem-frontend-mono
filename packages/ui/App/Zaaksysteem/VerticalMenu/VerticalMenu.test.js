import React from 'react';
import toJson from 'enzyme-to-json';
import { VerticalMenu } from './VerticalMenu';
import { shallow } from 'enzyme';

/**
 * @test {VerticalMenu}
 */
describe('The `VerticalMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(<VerticalMenu items={items} classes={{}} />);

    expect(toJson(component)).toMatchSnapshot();
  });
});
