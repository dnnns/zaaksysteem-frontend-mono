import React from 'react';
import { withStyles } from '@material-ui/styles';
import { communicationStyleSheet } from './Communication.style';
import Render from '@mintlab/ui/App/Abstract/Render';
import ThreadsContainer from './Threads/ThreadsContainer';
import { Route, Switch } from 'react-router-dom';
import ThreadContainer from './Thread/ThreadContainer';
import AddForm from './AddForm/AddForm';
import { compose } from 'recompose';
import withWidth from '@material-ui/core/withWidth';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import ThreadPlaceholder from './ThreadPlaceholder/ThreadPlaceholder';

const Communication = ({ classes, width, rootPath, requestor }) => {
  const threadsBreakpoint = ['md', 'lg', 'xl'].includes(width);

  if (!requestor) {
    return <Loader delay="200" />;
  }

  return (
    <div className={classes.wrapper}>
      <Route
        path={`${rootPath}/(view|new)?/:identifier?`}
        render={({ match }) => {
          const renderThreads =
            !match.params.identifier ||
            (match.params.identifier && threadsBreakpoint);

          return (
            <Render condition={renderThreads}>
              <ThreadsContainer match={match} basePath={rootPath} />
            </Render>
          );
        }}
      />

      <Switch>
        <Route
          path={`${rootPath}/new/:type`}
          render={props => (
            <div className={classes.contentOuterWrapper}>
              <AddForm {...props} rootPath={rootPath} width={width} />
            </div>
          )}
        />
        <Route
          path={`${rootPath}/view/:threadId`}
          render={props => (
            <div className={classes.contentOuterWrapper}>
              <ThreadContainer {...props} rootPath={rootPath} width={width} />
            </div>
          )}
        />
        <Route
          path={`${rootPath}`}
          render={() => (
            <Render condition={threadsBreakpoint}>
              <div className={classes.contentOuterWrapper}>
                <ThreadPlaceholder />
              </div>
            </Render>
          )}
        />
      </Switch>
    </div>
  );
};

export default compose(
  withStyles(communicationStyleSheet),
  withWidth()
)(Communication);
