import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const errorBoundaryStylesheet: StyleRulesCallback<any, {}> = () => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
  },
  titleWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontWeight: 600,
  },
  icon: {
    marginRight: 20,
  },
  stacktrace: {
    whiteSpace: 'pre-line',
  },
});
