import React from 'react';
import { iterator } from '../library/iterator';
import style from './Fold.module.css';
import { addScopeAttribute } from '../../../library/addScope';

const LENGTH = 4;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Fold = ({ color, scope }) => (
  <div className={style['fold']} {...addScopeAttribute(scope)}>
    {iterator(LENGTH).map(item => (
      <div key={item} className={style['cube']}>
        <span
          className={style['square']}
          style={{
            backgroundColor: color,
          }}
        />
      </div>
    ))}
  </div>
);
