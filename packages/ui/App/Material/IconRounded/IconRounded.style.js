/**
 * Style Sheet with theme access for the {@link IconRounded} component.
 * See the CSS file for the autoprefixed CSS grid.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const iconRoundedStyleSheet = () => ({
  icon: {
    margin: 'auto',
  },
});
