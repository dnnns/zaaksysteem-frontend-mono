import React from 'react';
//@ts-ignore
import { H4 } from '@mintlab/ui/App/Material/Typography';

type ThreadTitlePropsType = {
  classes: any;
  title: string;
};

const ThreadTitle: React.FunctionComponent<ThreadTitlePropsType> = ({
  classes,
  title,
}) => <H4 classes={classes}>{title}</H4>;

export default ThreadTitle;
