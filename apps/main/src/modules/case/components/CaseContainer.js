import { connect } from 'react-redux';
import Case from './Case';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

const mapStateToProps = state => {
  if (!state.case || state.case.state !== AJAX_STATE_VALID) {
    return {
      busy: true,
    };
  }

  const {
    session: {
      data: {
        logged_in_user: { display_name: name },
      },
    },
    case: {
      id: caseUuid,
      requestor: { reference: contactUuid, display_name: contactName },
    },
  } = state;

  return { name, caseUuid, contactUuid, contactName };
};

const CaseContainer = connect(mapStateToProps)(Case);

export default CaseContainer;
