/* eslint-disable no-console */
const glob = require('glob');
const { promisify } = require('util');
const { exec } = require('child_process');

const asyncGlob = promisify(glob);
const asyncExec = promisify(exec);

async function countLinesInFiles(files) {
  const contents = await Promise.all(
    files.map(filePath => asyncExec(`cat ${filePath} | wc -l`))
  );
  return contents
    .map(({ stdout }) => parseInt(stdout, 10))
    .reduce((acc, num) => acc + num, 0);
}

async function printStats() {
  const jsFiles = await asyncGlob('src/**/*.js');
  const tsFiles = await asyncGlob('src/**/*.ts?(x)');
  const jsLOC = await countLinesInFiles(jsFiles);
  const tsLOC = await countLinesInFiles(tsFiles);
  const totalLOC = jsLOC + tsLOC;
  const progress = Math.round((tsLOC / totalLOC) * 100) / 2;

  const progressBar = Array(50)
    .fill(null)
    .map((value, index) => (index < progress ? `\u2588` : `\u2591`))
    .join('');

  console.log(`TypeScript progress: ${progressBar} ${progress * 2}%`);
  console.log(
    `Total lines: ${totalLOC}, JavaScript lines: ${jsLOC}, TypeScript lines: ${tsLOC}\n`
  );
}

printStats();
