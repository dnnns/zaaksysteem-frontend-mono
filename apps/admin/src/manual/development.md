# Development

> There are two distinct development activities 
  that are spread out over the 
  [two local sibling working copies](./docker.html#vcs-working-copy-dependencies).

## The `./zaaksysteem-apps/` working copy

Edit the source code in `./apps`. Sweet and simple.

But for that to take any effect, there is something else 
you need to do, somewhere else, first.

## The `./zaaksysteem/` working copy

In the main zaaksysteem working copy, you need to open an 
interactive shell for the `apps` service development container 
as the `node` user, using the container initialization file under 
`./zaaksysteem-apps/node/bash/`.

### One-time setup

Since that is tedious to remember and type, create 
an alias in your host shell initialization file:

    alias apps="docker-compose exec -u node apps bash --init-file bash/node.bashrc"

The next time your host initialization file is read, you can run

    $ apps

in the `./zaaksysteem` directory to enter the container.

Your `PS1` will be a happy little whale, and you can now 
run development commands.
 
## Development commands

The `apps` service development container is based on an official
Node.js Docker image that contains both the `npm` and `yarn` 
executables, neither of which you will use directly.

Instead, use the `zs` executable.

Your most common task will be to start the development server:

    🐳 zs start

Other common commands are

    🐳 zs lint
    🐳 zs test

You can get an interactive prompt with a 
list of all sub commands by just running

    🐳 zs
