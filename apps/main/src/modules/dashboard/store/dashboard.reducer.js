import { AJAX_STATE_INIT } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import handleAjaxStateChange from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { FETCH_OPEN_CASES } from './dashboard.contstants';

const initialState = {
  cases: [],
  state: AJAX_STATE_INIT,
};

const normalizeCases = response => {
  const { result } = response;
  return result.map(item => ({
    id: item.id,
    caseType: item.values['case.casetype.name'],
    caseNumber: item.values['case.number'],
  }));
};

const handleOpenCasesStateChange = handleAjaxStateChange(FETCH_OPEN_CASES);

export const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_OPEN_CASES.PENDING:
    case FETCH_OPEN_CASES.ERROR:
      return {
        ...state,
        ...handleOpenCasesStateChange(state, action),
      };

    case FETCH_OPEN_CASES.SUCCESS:
      return {
        ...handleOpenCasesStateChange(state, action),
        cases: normalizeCases(action.payload.response),
      };

    default:
      return state;
  }
};
