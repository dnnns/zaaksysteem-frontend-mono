import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '../../communication';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';

const Case = ({ caseUuid, contactUuid, contactName, busy }) => {
  if (busy) {
    return <Loader />;
  }

  return (
    <React.Fragment>
      <Switch>
        <Route
          path={`/:prefix/case/:caseId/communication`}
          render={({ match }) => (
            <CommunicationModule
              type="case"
              caseUuid={caseUuid}
              contactUuid={contactUuid}
              contactName={contactName}
              rootPath={match.url}
            />
          )}
        />
      </Switch>
    </React.Fragment>
  );
};

export default Case;
