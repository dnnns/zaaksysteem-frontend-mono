import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const THREAD_FETCH = createAjaxConstants('THREAD_FETCH');
