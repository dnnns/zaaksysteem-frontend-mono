# 🔌 `DropdownMenu` component

> Dropdown Menu component. 
> 
> The `items` prop may be either an array of objects in this form:

```
  [{
    action: action('Foo Action'),
    icon: 'menu',
    label: 'First menu item with icon',
  }]
```

or, an array of these arrays. In that case, each group in the Dropdown Menu will be divided by a separator.

## See also

- [`DropdownMenu` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/DropdownMenu)
- [`DropdownMenu` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-dropdownmenu)
