import { StyleRulesCallback } from '@material-ui/core';

/**
 * Communication layouts
 *
 * @return {JSS}
 */
export const communicationStyleSheet: StyleRulesCallback<any, {}> = ({
  breakpoints,
}) => ({
  wrapper: {
    height: '100vh',
    display: 'flex',
    '&>:first-child': {
      width: '100%',
      height: '100%',
    },
    '&>:nth-child(2)': {
      flex: 1,
      height: '100%',
    },
    [breakpoints.up('md')]: {
      '&>:first-child': {
        maxWidth: '420px',
      },
    },
    [breakpoints.up('lg')]: {
      '&>:first-child': {
        maxWidth: '600px',
      },
    },
  },
  contentOuterWrapper: {
    height: '100%',
    display: 'flex',
    overflowY: 'auto',
    padding: '30px',
    justifyContent: 'center',
  },
});
