import { dashboardReducer } from './dashboard.reducer';
import { fetchOpenCases } from './dashboard.actions';

export function getDashboardModule(userId) {
  return {
    id: 'dashboard',
    reducerMap: {
      dashboard: dashboardReducer,
    },
    initialActions: [fetchOpenCases(userId)],
  };
}
