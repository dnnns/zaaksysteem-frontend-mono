import React from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';

import { stories } from '../../storybook/story';
import FormRenderer from './FormRenderer';
import * as fieldTypes from './constants/fieldTypes';
import { Rule, hasValue, hideFields, showFields, setFieldValue } from './rules';

const formDefinition = [
  {
    name: 'first_name',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    placeholder: 'Enter your first name',
  },
  {
    name: 'last_name',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    placeholder: 'Enter your last name',
  },
  {
    name: 'test',
    type: fieldTypes.CHOICE_CHIP,
    value: '',
    choices: [
      {
        label: 'Choice A',
        value: 'a',
      },
      {
        label: 'Choice B',
        value: 'b',
      },
    ],
    required: true,
  },
  {
    name: 'email_address',
    type: fieldTypes.EMAIL,
    value: '',
    required: true,
    placeholder: 'e.g. example@fake.com',
  },
  {
    name: 'message',
    type: fieldTypes.TEXTAREA,
    value: '',
    required: true,
    placeholder:
      'This value is set by rule engine when you fill out the first name field',
    isMultiline: true,
  },
  {
    name: 'hidden',
    type: fieldTypes.TEXTAREA,
    value: '',
    required: true,
    placeholder:
      'This field is hidden when you fill out first name using the rule engine',
    isMultiline: true,
  },
];

const rules = [
  new Rule('first_name')
    .when(hasValue)
    .then(hideFields(['hidden']))
    .and(setFieldValue('message', 'Filled by rule engine'))
    .else(showFields(['hidden']))
    .and(setFieldValue('message', '')),
];

const renderField = ({ FieldComponent, name, ...rest }, errors, touched) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    compact: true,
    name,
    scope: `email-template-form-component-${name}`,
  };

  return (
    <div key={`email-template-form-component-${name}`} style={{ padding: 5 }}>
      <FieldComponent {...props} />
      {errors[name] && touched[name] && <p>{errors[name]}</p>}
    </div>
  );
};

stories(module, __dirname, {
  Default() {
    return (
      <FormRenderer formDefinition={formDefinition} rules={rules}>
        {({ fields, errors, touched }) => {
          return fields.map(field => renderField(field, errors, touched));
        }}
      </FormRenderer>
    );
  },
});
