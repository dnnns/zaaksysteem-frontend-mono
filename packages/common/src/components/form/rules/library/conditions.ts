import deepEqual from 'fast-deep-equal';
import { FormValue, FlatFormValue } from '../../types/formDefinition.types';

type CompareValue = string | number | string[] | number[];

export type RuleEngineCondition = (value: FormValue) => boolean;
export type RuleEngineCompareCondition = (
  conditionValue: CompareValue
) => RuleEngineCondition;

export type RuleEngineCreateCondition =
  | RuleEngineCondition
  | RuleEngineCompareCondition;

function normalizeValue(value: FormValue): FlatFormValue {
  return typeof value === 'string' || typeof value === 'number'
    ? value
    : value.value;
}

export const valueEquals: RuleEngineCompareCondition = equals => value =>
  deepEqual(equals, normalizeValue(value));

export const hasValue: RuleEngineCondition = value =>
  value.toString().trim().length > 0;

export const valueOneOf: RuleEngineCompareCondition = equals => value => {
  return Array.isArray(equals)
    ? equals.includes(normalizeValue(value) as any)
    : false;
};

export const valueBiggerThan: RuleEngineCompareCondition = biggerThan => value =>
  value > biggerThan;

export const valueSmallerThan: RuleEngineCompareCondition = smallerThan => value =>
  value < smallerThan;
