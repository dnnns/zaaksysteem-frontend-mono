import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const messageContentStyleSheet: StyleRulesCallback<any, {}> = ({
  mintlab: { greyscale },
}) => ({
  content: {
    backgroundColor: greyscale.dark,
    color: greyscale.offblack,
    marginTop: '20px',
    marginLeft: '80px',
    padding: '20px',
    borderRadius: 8,
    whiteSpace: 'pre-line',
  },
});
