import React, { useEffect, useState } from 'react';
import { AutoSizer, List } from 'react-virtualized';
import { withStyles } from '@material-ui/styles';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { Body1 } from '@mintlab/ui/App/Material/Typography';
import { NewGenericTextField } from '@mintlab/ui/App/Material/TextField';
import Icon from '@mintlab/ui/App/Material/Icon';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  addScopeProp,
  addScopeAttribute,
} from '@mintlab/ui/App/library/addScope';
import NoteThreadListItem from '../ThreadListItem/NoteThreadListItem';
import ContactMomentThreadListItem from '../ThreadListItem/ContactMomentThreadListItem';
import {
  TYPE_NOTE,
  TYPE_CONTACT_MOMENT,
} from '../../library/communicationTypes.constants';
import { threadsStyleSheet } from './Threads.style';
import ErrorBoundary from '../../../../components/ErrorBoundary/ErrorBoundary';

const components = {
  [TYPE_NOTE]: NoteThreadListItem,
  [TYPE_CONTACT_MOMENT]: ContactMomentThreadListItem,
};

const filterThreads = (threads, searchTerm, filter) => {
  const matchField = (obj, field) =>
    obj[field]
      ? obj[field].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      : false;

  return threads
    .filter(thread => (filter === null ? true : thread.type === filter.value))
    .filter(thread =>
      searchTerm === ''
        ? true
        : ['summary', 'withName', 'createdByName'].some(field =>
            matchField(thread, field)
          )
    );
};

const Threads = ({
  classes,
  threads,
  match,
  basePath,
  fetchThreads,
  busy,
  rootPath,
}) => {
  const [t] = useTranslation(['communication', 'common']);
  const [searchTerm, setSearchTerm] = useState('');
  const [filter, setFilter] = useState(null);
  const filteredThreads = filterThreads(threads, searchTerm, filter);
  const addThreadPath = `${rootPath}/new/${TYPE_CONTACT_MOMENT}`;
  const filterChoices = [TYPE_CONTACT_MOMENT, TYPE_NOTE].map(type => ({
    label: t(`threadTypes.${type}`),
    value: type,
  }));

  const rowRenderer = ({ key, index, style }) => {
    const data = filteredThreads[index];
    const { id, type } = data;
    const selected = match.params.identifier === id;
    const props = {
      key,
      selected,
      basePath,
      ...data,
    };
    const Component = components[type];

    return <Component {...props} style={style} />;
  };

  const handleTextFieldChange = event => setSearchTerm(event.target.value);

  const clearTextField = () => setSearchTerm('');

  const handleFilterChange = event => setFilter(event.target.value);

  const renderList = () => (
    <div className={classes.listWrapper}>
      <AutoSizer>
        {({ width, height }) => (
          <List
            height={height}
            rowCount={filteredThreads.length}
            rowHeight={110}
            rowRenderer={rowRenderer}
            width={width}
          />
        )}
      </AutoSizer>
    </div>
  );

  const renderNoContent = () => (
    <div className={classes.noContentWrapper}>
      <Body1 classes={{ root: classes.noContentText }}>
        {t('thread.noContent')}
      </Body1>
    </div>
  );

  const renderContent = () =>
    filteredThreads.length > 0 ? renderList() : renderNoContent();

  const LinkComponent = React.forwardRef(({ children, ...restProps }, ref) => (
    <Link innerRef={ref} {...restProps} to={addThreadPath}>
      {children}
    </Link>
  ));
  LinkComponent.displayName = 'addButtonLinkComponent';

  useEffect(() => {
    fetchThreads();
  }, []);

  return (
    <div className={classes.wrapper}>
      <div className={classes.actionsWrapper}>
        <div className={classes.buttonActionWrapper}>
          <Button
            component={LinkComponent}
            presets={['primary', 'medium']}
            icon="add"
            {...addScopeProp('add-thread')}
          >
            {t('common:forms.add')}
          </Button>
        </div>
        <div
          className={classes.filterActionWrapper}
          {...addScopeAttribute('filter-thread')}
        >
          <Select
            generic={true}
            value={filter}
            isClearable={true}
            isSearchable={false}
            placeholder="Filter"
            choices={filterChoices}
            onChange={handleFilterChange}
          />
        </div>
      </div>
      <div className={classes.searchWrapper}>
        <NewGenericTextField
          startAdornment={
            <div className={classes.searchIcon}>
              <Icon size="small" color="inherit">
                search
              </Icon>
            </div>
          }
          placeholder={t('thread.searchPlaceholder')}
          onChange={handleTextFieldChange}
          value={searchTerm}
          closeAction={searchTerm && clearTextField}
          {...addScopeProp('search-thread')}
        />
      </div>
      {busy ? (
        <Loader delay="200" />
      ) : (
        <ErrorBoundary>{renderContent()}</ErrorBoundary>
      )}
    </div>
  );
};

export default withStyles(threadsStyleSheet)(Threads);
