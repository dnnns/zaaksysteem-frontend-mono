const formDefinition = [
  {
    name: 'name',
    type: 'text',
    value: '',
    required: true,
    label: 'folder:fields.name.label',
    placeholder: 'folder:fields.name.label',
  },
];

export default formDefinition;
