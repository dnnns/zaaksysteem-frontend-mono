import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { FETCH_OPEN_CASES } from './dashboard.contstants';

const fetchOpenCasesAction = createAjaxAction(FETCH_OPEN_CASES);

const createQuery = userId => {
  const query = `SELECT case.status, case.num_unaccepted_updates, case.num_unaccepted_files, case.number, case.progress_status, case.casetype.name, case.subject, case.requestor.name, case.days_left, case.archival_state, case.destructable, case.casetype.id, case.requestor.id, case.assignee.id FROM case WHERE (case.assignee.id = ${userId}) AND (case.status = "open") NUMERIC ORDER BY case.number DESC`;

  return query.replace(/\s/g, '+');
};

export const fetchOpenCases = userId =>
  fetchOpenCasesAction({
    url: `/api/object/search?zapi_num_rows=10&zapi_page=1&zql=${createQuery(
      userId
    )}`,
    method: 'GET',
  });
