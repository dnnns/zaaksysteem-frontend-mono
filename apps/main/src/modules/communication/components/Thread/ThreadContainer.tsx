import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import Thread, { ThreadPropsType } from './Thread';
import { fetchThreadAction } from '../../store/thread/communication.thread.actions';
import { CommunicationRootStateType } from '../../store/communication.reducer';

type PropsFromStateType = Pick<ThreadPropsType, 'messages' | 'busy'>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      thread: { messages, state },
    },
  } = stateProps;

  if (!messages) return { messages: [], busy: false };

  return { messages, busy: state !== AJAX_STATE_VALID };
};

type PropsFromDispatchType = Pick<ThreadPropsType, 'fetchThread'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  fetchThread(payload: string) {
    const action = fetchThreadAction(payload);
    if (action !== null) {
      dispatch(action as any);
    }
  },
});

const ThreadContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(Thread);

export default ThreadContainer;
