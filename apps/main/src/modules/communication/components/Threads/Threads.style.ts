import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const threadsStyleSheet: StyleRulesCallback<any, {}> = ({
  mintlab: { greyscale },
  palette: { primary },
}) => ({
  wrapper: {
    height: '100%',
    overflowY: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    borderRight: `1px solid ${greyscale.dark}`,
  },
  noContentWrapper: {
    padding: 10,
    margin: 10,
    backgroundColor: primary.lighter,
    borderRadius: 5,
  },
  noContentText: {
    color: greyscale.darkest,
  },
  actionsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    padding: 18,
    paddingBottom: 9,
  },
  buttonActionWrapper: {
    flexGrow: 1,
  },
  filterActionWrapper: {
    width: 200,
  },
  searchWrapper: {
    borderTop: `1px solid ${greyscale.dark}`,
    padding: 9,
  },
  listWrapper: {
    flexGrow: 1,
    borderTop: `1px solid ${greyscale.dark}`,
  },
  searchIcon: {
    color: greyscale.darkest,
  },
});
