import errorMiddleware from './error.middleware';

export const getErrorModule = () => ({
  id: 'error',
  middlewares: [errorMiddleware],
});

export default getErrorModule;
