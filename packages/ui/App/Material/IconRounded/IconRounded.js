import React from 'react';
import { withStyles } from '@material-ui/styles';
import { withTheme } from '@material-ui/styles';
import Icon from './../Icon';
import { iconRoundedStyleSheet } from './IconRounded.style';

/**
 * *Material Design* **RoundedIcon**.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/RoundedIcon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const IconRounded = props => {
  const { classes, theme, size, backgroundColor } = props;
  const backgroundSize = Number.isInteger(size)
    ? size
    : theme.mintlab.icon[size];
  const iconSize = Math.floor(backgroundSize * 0.65);

  return (
    <Icon
      {...props}
      classes={{ root: classes.icon }}
      size={iconSize}
      style={{
        borderRadius: '50%',
        width: `${backgroundSize}px`,
        height: `${backgroundSize}px`,
        backgroundColor,
        ...props.style,
      }}
    >
      {props.children}
    </Icon>
  );
};

export default withTheme(withStyles(iconRoundedStyleSheet)(IconRounded));
