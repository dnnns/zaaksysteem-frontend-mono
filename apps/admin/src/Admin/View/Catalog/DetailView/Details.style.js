/**
 * @param {Object} theme
 * @return {JSS}
 */
export const detailsStyleSheet = ({ mintlab: { greyscale }, typography }) => ({
  wrapper: {
    margin: '30px 0px',
    position: 'relative',
  },
  header: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    width: '50px',
    height: '28px',
    color: greyscale.evenDarker,
  },
  title: {
    flexGrow: '1',
    color: greyscale.evenDarker,
    fontSize: typography.subtitle2.fontSize,
  },
  content: {
    marginLeft: '50px',
    display: 'flex',
    alignItems: 'flex-start',
  },
  values: {
    margin: '4px 0px',
    overflow: 'hidden',
  },
  valueActionButton: {
    padding: '4px',
    marginLeft: '6px',
  },
});
