import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { THREAD_FETCH } from './communication.thread.constants';
import { APIGetMessageListResponseBody } from './../../../../_generated/Communication.types';
import { AnyMessageType, TypeOfMessage } from './../../types/Message.types';

export interface CommunicationThreadState {
  state: AjaxState;
  caseRelation: null | {
    id: string;
    caseNumer: string;
  };
  messages?: AnyMessageType[];
}

const initialState: CommunicationThreadState = {
  state: AJAX_STATE_INIT,
  caseRelation: null,
  messages: undefined,
};

const getCaseRelationship = (response: APIGetMessageListResponseBody): any => {
  return response.relationships && response.relationships.case
    ? {
        id: response.relationships.id,
        caseNumber: response.relationships.case.meta.display_id,
      }
    : null;
};

const handleFetchSuccess = (
  state: CommunicationThreadState,
  action: AjaxAction<APIGetMessageListResponseBody>
): CommunicationThreadState => {
  const messages = action.payload.response.data;

  if (!messages) {
    return state;
  }

  return {
    ...state,
    caseRelation: getCaseRelationship(action.payload.response),
    messages: messages.map(message => {
      const { type, id = '' } = message;
      const { channel, direction, content } = message.attributes;
      const { created: createdDate } = message.meta;
      const createdBy = message.relationships.created_by;
      const recipient = message.relationships.recipient;

      return {
        type: type as TypeOfMessage,
        id,
        channel,
        direction,
        content,
        createdDate,
        sender: {
          id: createdBy.id,
          name: createdBy.name,
        },
        recipient: {
          id: recipient.id,
          name: recipient.name,
        },
      };
    }),
  };
};

export const threads: Reducer<CommunicationThreadState, AjaxAction<unknown>> = (
  state = initialState,
  action
) => {
  const { type } = action;

  const handleFetchThreadsStateChange = handleAjaxStateChange(THREAD_FETCH);

  switch (type) {
    case THREAD_FETCH.PENDING:
    case THREAD_FETCH.ERROR:
      return handleFetchThreadsStateChange(state, action);
    case THREAD_FETCH.SUCCESS:
      return handleFetchThreadsStateChange(
        handleFetchSuccess(state, action as AjaxAction<
          APIGetMessageListResponseBody
        >),
        action
      );
    default:
      return state;
  }
};

export default threads;
