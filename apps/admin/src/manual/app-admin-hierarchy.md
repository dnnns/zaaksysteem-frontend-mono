# Admin: App Hierarchy

The app is bootstrapped in `/apps/Admin/View/render.js` with an entry point that is composed of the components under `/apps/Admin/View/Shared/App` as follows:

- `Provider` (> `redux` > `@material-ui`)
    - `ErrorBoundary` (> `../ResourceContainer`)
        - `Locale` (> `react-i18next`)
            - `LoginContainer`
                - `Login`
                    - `LayoutContainer` 
                        - `Layout`
                            - `ErrorBoundary`
                                - `View`

The nesting order reflects the business goal priority:

1. We need a store for resources and a theme for exceptions
2. We want a top level error boundary and need a resource for login
3. We need a locale for GUI feedback
4. We need to handle authentication and/or authorization
5. We need a global layout
6. We want a view level error boundary and need to render the view
