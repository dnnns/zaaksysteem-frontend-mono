import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const noteFormDefinition = [
  {
    name: 'content',
    type: fieldTypes.TEXT,
    multi: true,
    value: '',
    required: true,
    placeholder: 'communication:addFields.note',
    isMultiline: true,
    rows: 7,
  },
];

export default noteFormDefinition;
