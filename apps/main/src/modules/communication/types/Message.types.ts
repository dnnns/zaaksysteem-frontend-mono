type Direction = 'incoming' | 'outgoing';
export type TypeOfMessage = 'contact_moment' | 'note';

type NameObject = { name: string };

interface MessageType {
  id: string;
  type: TypeOfMessage;
  content: string;
  createdDate: string;
  sender: NameObject;
}

export interface ContactMomentMessageType extends MessageType {
  channel: string;
  direction: Direction;
  recipient: NameObject;
}

export interface NoteMessageType extends MessageType {}

export type AnyMessageType = ContactMomentMessageType | NoteMessageType;
