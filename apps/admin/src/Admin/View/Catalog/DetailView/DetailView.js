import React from 'react';
import { withStyles } from '@material-ui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import { H3 } from '@mintlab/ui/App/Material/Typography';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import { detailViewStyleSheet } from './DetailView.style';
import ColoredIcon from './../../Shared/Icons/ColoredIcon';
import Detail from './Detail';

const createDetailList = (details, t) => {
  if (!details || !details.length) {
    return null;
  }

  return details.map(detail => (
    <Detail key={detail.type} detail={detail} t={t} />
  ));
};

/**
 * @reactProps {Object} classes
 * @reactProps {Function} closeDetailView
 * @reactProps {Object} detailInfo
 * @reactProps {string} detailInfo.icon
 * @reactProps {string} detailInfo.name
 * @reactProps {string} detailInfo.type
 * @reactProps {Object} detailInfo.details
 * @reactProps {Object} detailInfo.relations
 * @reactProps {string} detailInfo.version
 * @reactProps {boolean} loading
 * @reactProps {Function} t
 * @return {ReactElement}
 */
const DetailView = ({
  classes,
  closeDetailView,
  detailInfo: { icon, name, type, details, relations, version },
  loading,
  t,
}) => (
  <div className={classes.detailView}>
    <div className={classes.header}>
      <div className={classes.headerIcon}>
        <ColoredIcon size="large" value={icon} />
      </div>
      <H3
        classes={{
          root: classes.title,
        }}
      >
        {name}
      </H3>
      <div>
        <Button
          action={closeDetailView}
          presets={['icon', 'large']}
          scope="catalog-detail:close"
        >
          close
        </Button>
      </div>
    </div>
    <Render condition={type === 'case_type'}>
      <div className={classes.subHeader}>
        <div className={classes.version}>
          {`${t('catalog:detailView:versionTitle')} ${version}`}
        </div>
      </div>
    </Render>
    <div className={classes.content}>
      {loading ? (
        <Loader />
      ) : (
        <div>
          {createDetailList(details, t)}
          <Render condition={relations && relations.length}>
            <div className={classes.relationTitle}>
              {t('catalog:detailView:relationsTitle')}
            </div>
            {createDetailList(relations, t)}
          </Render>
        </div>
      )}
    </div>
  </div>
);

export default withStyles(detailViewStyleSheet)(DetailView);
