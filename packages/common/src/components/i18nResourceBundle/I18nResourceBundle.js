import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

export const addResourceBundle = (i18n, namespace, locale) =>
  Object.entries(locale).forEach(([lng, translations]) => {
    i18n.addResourceBundle(lng, namespace, translations);
  });

export const removeResourceBundle = (i18n, namespace, locale) =>
  Object.entries(locale).forEach(([lng, translations]) =>
    i18n.removeResourceBundle(lng, namespace, translations)
  );

const I18nResourceBundle = ({ resource, namespace, children }) => {
  const [, i18n] = useTranslation();

  useEffect(() => {
    addResourceBundle(i18n, namespace, resource);

    return () => {
      removeResourceBundle(i18n, namespace, resource);
    };
  }, [resource]);

  return children;
};

export default I18nResourceBundle;
