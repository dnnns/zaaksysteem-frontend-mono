import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const threadPlaceholderStyleSheet: StyleRulesCallback<any, {}> = ({
  mintlab: { greyscale },
}) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  placeholder: {
    color: greyscale.offBlack,
  },
});
