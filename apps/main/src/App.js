import React, { useEffect } from 'react';
import { hot } from 'react-hot-loader/root';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import { useTranslation } from 'react-i18next';
import { configureStore, history } from './configureStore';
import Routes from './Routes';
import DialogRenderer from './components/DialogRenderer/DialogRenderer';
import ErrorDialog from './components/ErrorDialog/ErrorDialog';
import useDialogs from './library/useDialogs';
import useMessages from './library/useMessages';
import { DIALOG_ERROR } from './constants/dialog.constants';

const initialState = {};
const store = configureStore(initialState);

function App() {
  const [t] = useTranslation('common');
  const [, addDialogs] = useDialogs();
  const [, addMessages] = useMessages();

  useEffect(() => {
    addMessages(
      t('serverErrors.status', {
        returnObjects: true,
      })
    );

    addDialogs({
      [DIALOG_ERROR]: ErrorDialog,
    });
  }, []);

  return (
    <Provider store={store}>
      <MaterialUiThemeProvider>
        <ConnectedRouter history={history}>
          <Routes prefix={process.env.APP_CONTEXT_ROOT} />
        </ConnectedRouter>
        <DialogRenderer />
      </MaterialUiThemeProvider>
    </Provider>
  );
}

export default process.env.NODE_ENV === 'development' ? hot(App) : App;
