import React from 'react';
import { withStyles } from '@material-ui/styles';
import { valuesStyleSheet } from './Values.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Object} t
 * @return {ReactElement}
 */
const Default = ({ classes, value }) => (
  <div className={classes.default}>
    <span>{value}</span>
  </div>
);

export default withStyles(valuesStyleSheet)(Default);
