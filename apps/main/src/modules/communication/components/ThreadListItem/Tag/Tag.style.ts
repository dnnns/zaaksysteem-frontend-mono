import { StyleRulesCallback } from '@material-ui/core';

/**
 * @return {JSS}
 */
export const tagStyleSheet: StyleRulesCallback<any, {}> = ({
  palette: { primary },
}) => ({
  wrapper: {
    backgroundColor: primary.light,
    color: primary.main,
    fontWeight: 'bold',
    padding: '4px 14px',
    width: 'min-content',
    fontSize: '12px',
    textTransform: 'uppercase',
    borderRadius: '18px',
  },
});
