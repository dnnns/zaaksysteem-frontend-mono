import * as yup from 'yup';
import { TFunction } from 'i18next';
import { FormDefinitionField } from '../types/formDefinition.types';

/**
 * Validation rules for basic component types.
 * Note that rules are responsible for implementing the
 * `required` check, as the order of checks can matter.
 */

type ValidationRuleOptions = {
  t: TFunction;
  field: FormDefinitionField;
};

export type ValidationRule<S = yup.Schema<any>> = (
  options: ValidationRuleOptions
) => S;

export const emailRule: ValidationRule<yup.StringSchema> = ({ t, field }) => {
  const base = yup.string();
  const { required } = field;
  const message = t('validations:email.invalidEmailAddress');

  return required ? base.required().email(message) : base.email(message);
};

export const selectRule: ValidationRule<yup.MixedSchema> = ({ field }) =>
  field.required ? yup.mixed().required() : yup.mixed();

export const textRule: ValidationRule<yup.StringSchema> = ({ field }) =>
  field.required ? yup.string().required() : yup.string();
