import { EMAIL, SELECT, TEXT } from '../Constants/fieldTypes';
import * as validationRules from './validationRules';

export default {
  [EMAIL]: validationRules.emailRule,
  [SELECT]: validationRules.selectRule,
  [TEXT]: validationRules.textRule,
};
