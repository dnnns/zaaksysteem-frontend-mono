export const emailTemplate = {
  emailTemplate: {
    dialog: {
      title: 'Emailsjabloon {{action}}',
    },
    fields: {
      label: {
        label: 'Label',
      },
      sender: {
        label: 'Afzender',
        placeholder: 'Bijv: Afdeling Burgerzaken',
      },
      sender_address: {
        label: 'Antwoordadres',
        placeholder: 'Bijv: burgerzaken@gemeente.nl',
      },
      subject: {
        label: 'Onderwerp',
      },
      message: {
        label: 'Bericht',
      },
      attachments: {
        label: 'Bijlagen',
      },
      commit_message: {
        label: 'Opmerking',
        help: 'Korte omschrijving van de reden van aanmaak / wijziging.',
      },
    },
  },
};
